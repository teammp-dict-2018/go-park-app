const http = require('http');
const mysql = require('mysql');
const qs = require('querystring');

const conn = mysql.createConnection({ host : 'localhost', user : 'goparkadmin', password : 'goparkapp', database : 'goparkdb', multipleStatements: true });
conn.connect();

const handlers = {};

handlers["/"] = (req, res) => {
	res.end("Hello World!");
};

handlers["/login"] = (req, res) => {
	if(req.method == 'POST') {
		let body = '';
		req.on('data', data => { 
			body += data; 
		});
		req.on('end', () => {
			let jsonData = JSON.parse(body);
			let un = jsonData.username;
			let pw = jsonData.password;
			conn.query('SELECT * FROM `goparkusers` WHERE `username` = ? && `password` = ?', [un, pw], (err, results, fields) => {
				if(err) { console.log(err) };
				if(results.length > 0) {
					res.end("Login successful");
				}
				else {
					res.end("Username/Password does not match.");
				}
			});
		});
	}
};

handlers["/register"] = (req, res) => {
	if(req.method == 'POST') {
		let body = '';
		req.on('data', data => { 
			body += data; 
		});
		req.on('end', () => {
			let jsonData = JSON.parse(body);
			let fn = jsonData.firstname;
			let ln = jsonData.lastname;
			let un = jsonData.username;
			let pw = jsonData.password;
			let em = jsonData.email;
			let add = jsonData.address;
			let vt = jsonData.vehicletype;
			let pn = jsonData.platenumber;
			let query = "INSERT INTO goparkusers (firstname, lastname, username, password, email, address, vehicletype, platenumber) VALUES (?);";
			let userDetails = [fn, ln, un, pw, em, add, vt, pn];
			conn.query(query, [userDetails], (err, results, fields) => {
				if(err) { console.log(err) };
				if(results.affectedRows == 1) {
					console.log("Registration successful");
				}
			});
		});
		req.on('error', e => {
			console.error(`Get error: ${e.message}`);
		});
	}
}

http.createServer((req, res) => {
	if(handlers[req.url]) {
		res.writeHead(200, 'OK', { "Content-Type":"text/html" });
		handlers[req.url](req, res);
	}
	else {
		if(req.url.includes("?")) {
			let query = req.url.split("?");
			handlers[query[0]](req, res);
		}
		else {
			res.writeHead(404, {"Content-Type": "text/html"});
			res.end("404 Not Found");
		}
	}
}).listen(8080);
console.log("The server is now running");
