package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.EditText;
import android.widget.LinearLayout;

class CardHoldersNameTextField
{
	public EditText cardHoldersNameTextField(Context c) {
		LinearLayout.LayoutParams forCardHolder = new LinearLayout.LayoutParams(275, LinearLayout.LayoutParams.WRAP_CONTENT);
		GradientDrawable gd = new GradientDrawable();
		gd.setStroke(2, Color.parseColor("#3FB0AC"));
		EditText cardHolderField = new EditText(c);
		cardHolderField.setTextColor(Color.parseColor("#3FB0AC"));
		cardHolderField.setBackground(gd);
		cardHolderField.setBackgroundColor(Color.WHITE);
		cardHolderField.setLayoutParams(forCardHolder);
		return(cardHolderField);
	}
}
