package com.goparkapp;

import android.os.Bundle;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.view.GravityCompat;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.content.Intent;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class NavigationDrawer extends AppCompatActivity
{
	private DrawerLayout mDrawerLayout;
	private FrameLayout content;

	protected void onCreateDrawer(final int layoutResID) {
		setContentView(R.layout.drawer_layout);
		content = (FrameLayout) findViewById(R.id.content_frame);
		getLayoutInflater().inflate(layoutResID, content, true);
		mDrawerLayout = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setBackgroundColor(Color.WHITE);
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem) {
				menuItem.setChecked(true);
				System.out.println("item: " + menuItem.getItemId());
				Intent intent = null;
				switch(menuItem.getItemId()) {
					case R.id.nav_home:
						intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_account_settings:
						intent = new Intent(getApplicationContext(), AccountSettingsActivity.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_transactions:
						intent = new Intent(getApplicationContext(), TransactionsActivity.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_about:
						intent = new Intent(getApplicationContext(), AboutActivity.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_tos:
						intent = new Intent(getApplicationContext(), TermsOfService.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_privacy:
						intent = new Intent(getApplicationContext(), PrivacyPolicy.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_faq:
						intent = new Intent(getApplicationContext(), NeedHelp.class);
						startActivity(intent);
						finish();
						break;
					case R.id.nav_logout:
						FirebaseAuth.getInstance().signOut();
						intent = new Intent(getApplicationContext(), LoginActivity.class);
						startActivity(intent);
						finish();
						break;
				}
				return(true);
			}
		});
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setBackgroundColor(Color.parseColor("#3FB0AC"));
		ActionBar actionbar = getSupportActionBar();
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setHomeAsUpIndicator(R.drawable.burger);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				mDrawerLayout.openDrawer(GravityCompat.START);
				return(true);
		}
		return(super.onOptionsItemSelected(item));
	}
}
