package com.goparkapp;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.View;

public class NeedHelp extends NavigationDrawer
{
	private Button btnGoPark;
	private Button btnUsage;
	private Button btnFree;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.need_help_layout);
		LinearLayout mainLayout = (LinearLayout) findViewById(R.id.nh_layout);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		btnGoPark = (Button)findViewById(R.id.btnGoPark);
		btnUsage = (Button)findViewById(R.id.btnUsage);
		btnFree = (Button)findViewById(R.id.btnFree);
		final View panelGoPark = findViewById(R.id.panelGoPark);
		panelGoPark.setVisibility(View.GONE);
		final View panelUsage = findViewById(R.id.panelUsage);
		panelUsage.setVisibility(View.GONE);
		final View panelFree = findViewById(R.id.panelFree);
		panelFree.setVisibility(View.GONE);
		goParkBtn();
		usageBtn();
		freeUseBtn();
	}

	public void goParkBtn() {
		btnGoPark.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View panelGoPark = findViewById(R.id.panelGoPark);
				if(panelGoPark.getVisibility() == View.VISIBLE) {
					panelGoPark.setVisibility(View.GONE);
				}
				else {
					panelGoPark.setVisibility(View.VISIBLE);
				}
				View panelUsage = findViewById(R.id.panelUsage);
				panelUsage.setVisibility(View.GONE);
				View panelFree = findViewById(R.id.panelFree);
				panelFree.setVisibility(View.GONE);
			}
		});
	}

	public void usageBtn() {
		btnUsage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View panelUsage = findViewById(R.id.panelUsage);
				if(panelUsage.getVisibility() == View.VISIBLE) {
					panelUsage.setVisibility(View.GONE);
				}
				else {
					panelUsage.setVisibility(View.VISIBLE);
				}
				View panelGoPark = findViewById(R.id.panelGoPark);
				panelGoPark.setVisibility(View.GONE);
				View panelFree = findViewById(R.id.panelFree);
				panelFree.setVisibility(View.GONE);
			}
		});
	}

	public void freeUseBtn() {
		btnFree.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View panelFree = findViewById(R.id.panelFree);
				if(panelFree.getVisibility() == View.VISIBLE) {
					panelFree.setVisibility(View.GONE);
				}
				else {
					panelFree.setVisibility(View.VISIBLE);
				}
				View panelGoPark = findViewById(R.id.panelGoPark);
				panelGoPark.setVisibility(View.GONE);
				View panelUsage = findViewById(R.id.panelUsage);
				panelUsage.setVisibility(View.GONE);
			}
		});
	}
}
