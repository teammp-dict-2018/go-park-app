package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.widget.Button;
import android.widget.LinearLayout;

class ChangeButton
{
	public Button changeButton(Context c, LinearLayout.LayoutParams lp) {
		LinearLayout.LayoutParams forButton = lp;
		Button change = new Button(c);
		change.setText("FORGOT");
		change.setTextSize(14);
		change.setTextColor(Color.WHITE);
		change.setBackgroundColor(Color.parseColor("#919191"));
		change.setLayoutParams(forButton);
		return(change);
	}
}
