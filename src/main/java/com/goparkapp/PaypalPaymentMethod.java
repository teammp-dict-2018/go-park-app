package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

class PaypalPaymentMethod
{
	Context c;

	public PaypalPaymentMethod(Context c) {
		this.c = c;
	}

	public ImageView paypalImage() {
		ImageView paypalImageView = new ImageView(c);
		paypalImageView.setImageResource(R.drawable.paypal);
		LinearLayout.LayoutParams forPaypalImage = new LinearLayout.LayoutParams(90, 90);
		paypalImageView.setLayoutParams(forPaypalImage);
		return(paypalImageView);
	}

	public ImageView paypalImageClickEvent(ImageView paypal, final ImageView visa, final ImageView masterCard, final GradientDrawable gd, final GradientDrawable gd2, final GradientDrawable gd3) {
		final ImageView paypalImageView = paypal;
		paypalImageView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				gd3.setStroke(2, Color.WHITE);
				paypalImageView.setPadding(10, -300, 10, -300);
				gd.setStroke(0, Color.WHITE);
				visa.setPadding(0, 0, 0, 0);
				gd2.setStroke(0, Color.WHITE);
				masterCard.setPadding(0, 0, 0, 0);
				paypalImageView.setBackground(gd3);
			}
		});
		return(paypalImageView);
	}
}
