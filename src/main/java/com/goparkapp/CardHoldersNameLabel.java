package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

class CardHoldersNameLabel
{
	public TextView cardHoldersNameLabel(Context c, Typeface tf) {
		TextView cardNameLabel = new TextView(c);
		cardNameLabel.setText("CARD HOLDER'S NAME");
		cardNameLabel.setTypeface(tf);
		cardNameLabel.setTextSize(14);
		cardNameLabel.setTextColor(Color.WHITE);
		cardNameLabel.setPadding(0, 30, 0, 15);
		return(cardNameLabel);
	}
}
