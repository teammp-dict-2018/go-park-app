package com.goparkapp;

import android.content.Context;
import android.view.Gravity;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.Color;
import android.widget.TextView;

public class ParkingSlotTextStyle
{
	Context ctx;
	Typeface typeface;

	public ParkingSlotTextStyle(Context ctx) {
		this.ctx = ctx;
		typeface = Typeface.createFromAsset(ctx.getAssets(),"font/OpenSans.ttf");
	}

	public TextView textstyle(TextView tv) {
		tv.setTypeface(typeface);
		tv.setTextColor(Color.WHITE);
		tv.setPaintFlags(tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		tv.setPadding(38, 0, 38, 0);
		return(tv);
	}

	public TextView lowerTextstyle(TextView tv) {
		tv.setTypeface(typeface);
		tv.setTextColor(Color.WHITE);
		tv.setPaintFlags(tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		String text = tv.getText().toString();
		if(text.length() == 3) {
			tv.setPadding(32, 0, 30, 0);
		}
		else {
			tv.setPadding(38, 0, 38, 0);
		}
		return(tv);
	}
	
	public TextView mainTextStyles(TextView tv) {
		tv.setTypeface(typeface);
		tv.setGravity(Gravity.CENTER);
		tv.setTextSize(14);
		tv.setTextColor(Color.WHITE);
		return(tv);
	}
}