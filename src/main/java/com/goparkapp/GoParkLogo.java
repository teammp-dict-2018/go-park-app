package com.goparkapp;

import android.content.Context;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.LinearLayout;

class GoParkLogo
{
	public ImageView goParkLogo(Context c, int height) {
		ImageView goParkImage = new ImageView(c);
		goParkImage.setImageResource(R.drawable.gopark_logo);
		DisplayMetrics width = c.getResources().getDisplayMetrics();
		double imageWidth = width.widthPixels*0.5;
		goParkImage.setLayoutParams(new LinearLayout.LayoutParams((int)imageWidth, height));
		return(goParkImage);
	}
}
