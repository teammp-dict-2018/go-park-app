package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

class ForgotPasswordTextField
{
	public EditText forgotPasswordTextField(final Context c, Typeface tf, int width, int height, final Button changeButton) {
		final Popup popup = new Popup(c,tf);
		final CheckingSigns signs = new CheckingSigns();
		GradientDrawable gd = new GradientDrawable();
		gd.setStroke(2, Color.parseColor("#3FB0AC"));
		final EditText forgotPasswordField = new EditText(c);
		forgotPasswordField.setPadding(10, 3, 1, 3);
		forgotPasswordField.setBackground(gd);
		forgotPasswordField.setTextColor(Color.parseColor("#3FB0AC"));
		forgotPasswordField.setBackgroundColor(Color.WHITE);
		forgotPasswordField.setTextSize(14);
		forgotPasswordField.setHint("Your email");
		forgotPasswordField.setSingleLine();
		forgotPasswordField.setHintTextColor(Color.parseColor("#3FB0AC"));
		forgotPasswordField.setCompoundDrawablesWithIntrinsicBounds(R.drawable.aticon, 0, 0, 0);
		forgotPasswordField.setFilters(new InputFilter[] {
			new InputFilter.LengthFilter(51)
		});
		forgotPasswordField.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		forgotPasswordField.setLayoutParams(new LinearLayout.LayoutParams(width, height));
		forgotPasswordField.setCursorVisible(false);
		forgotPasswordField.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(v.getId() == forgotPasswordField.getId()) {
					forgotPasswordField.setHint("");
					forgotPasswordField.setCursorVisible(true);
				}
			}
		});
		Checking check = new Checking();
		check.checking(c, forgotPasswordField, changeButton, popup, signs);
		return(forgotPasswordField);
	}
}
