package com.goparkapp;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.View.OnClickListener;

public class ParkingSlot extends FrameLayout
{
	ImageView car;
	public static final int OCCUPIED = 0;
	public static final int AVAILABLE = 1;
	public static final int SELECTED = 2;
	int state;
	int index;

	public ParkingSlot(Context ctx) {
		super(ctx);
		car = new ImageView(ctx);
		LinearLayout.LayoutParams slotLayoutParams = new LinearLayout.LayoutParams(100, 100);
		car.setImageResource(R.drawable.bluecar);
		car.setPadding(29, 45, 31, 10);
		addView(car, LayoutParams.MATCH_PARENT);

	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return(index);
	}

	@Override
	public void onDraw(Canvas canvas) {
	}

	public void setClickHandler(final ParkingSlotClickListener pscl) {
		setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				if(pscl != null) {
					pscl.onParkingSlotClick(index, state);
				}
			}
		});
	}

	public void setState(int state) {
		if(state < OCCUPIED && state > SELECTED) {
			return;
		}
		this.state = state;
		if(this.state == AVAILABLE) {
			car.setImageResource(R.drawable.whitecar);
		}
		else if(this.state == OCCUPIED) {
			car.setImageResource(R.drawable.bluecar);
		}
		else if(this.state == SELECTED) {
			car.setImageResource(R.drawable.check);
		}
	}

	public static interface ParkingSlotClickListener {
		public void onParkingSlotClick(int index, int state);
	}
}
