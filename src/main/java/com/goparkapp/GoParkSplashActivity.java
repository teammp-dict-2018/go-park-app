package com.goparkapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class GoParkSplashActivity extends AppCompatActivity {
	private static int SPLASH_TIME_OUT = 2000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gopark_splash);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent teammpSplash = new Intent(GoParkSplashActivity.this, TeamMPSplashActivity.class);
				startActivity(teammpSplash);
				overridePendingTransition(R.anim.fadein, R.anim.fadeout);
				finish();
			}
		}, SPLASH_TIME_OUT);
	}
}
