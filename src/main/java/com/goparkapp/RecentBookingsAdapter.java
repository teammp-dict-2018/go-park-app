package com.goparkapp;

import android.view.LayoutInflater;
import android.widget.Toast;
import android.view.Gravity;
import android.view.View;
import android.content.Context;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.graphics.Typeface;
import android.graphics.Color;
import android.widget.TableRow.LayoutParams;
import android.widget.TableRow;

public class RecentBookingsAdapter extends ArrayAdapter<String>
{
	private Context context;
	private String[] mallNames;
	private String[] bDates;
	private String[] transactionId;
	private Typeface typeface;

	public RecentBookingsAdapter(Context c, String[] mallNames, String[] bDates, String[] transactionId) {
		super(c, R.layout.recent_booking_layout, mallNames);
		context = c;
		this.mallNames = mallNames;
		this.bDates = bDates;
		this.transactionId = transactionId;
	}

	public View getView(int pos, View view, ViewGroup parent) {
		typeface = Typeface.createFromAsset(context.getAssets(),"font/OpenSans.ttf");
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.recent_booking_layout, null, true);
		TableRow.LayoutParams mallParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		mallParams.setMargins(10, 20, 0, 20);
		mallParams.gravity = Gravity.CENTER;
		TextView mallName = (TextView) rowView.findViewById(R.id.mallname);
		mallName.setLayoutParams(mallParams);
		mallName.setTypeface(typeface);
		mallName.setText(mallNames[pos]);
		mallName.setWidth(250);
		mallName.setTextColor(Color.WHITE);
		TableRow.LayoutParams dateParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		dateParams.setMargins(0, 20, 0, 20);
		dateParams.gravity = Gravity.CENTER;
		TextView bookDate = (TextView) rowView.findViewById(R.id.bookDate);
		bookDate.setLayoutParams(dateParams);
		bookDate.setTypeface(typeface);
		bookDate.setText(bDates[pos]);
		bookDate.setWidth(250);
		bookDate.setTextColor(Color.WHITE);
		TableRow.LayoutParams transactParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		transactParams.setMargins(0, 20, 0, 10);
		transactParams.gravity = Gravity.CENTER;
		TextView trnsctnId = (TextView) rowView.findViewById(R.id.transactionId);
		trnsctnId.setLayoutParams(transactParams);
		trnsctnId.setTypeface(typeface);
		trnsctnId.setText(transactionId[pos]);
		trnsctnId.setWidth(250);
		trnsctnId.setTextColor(Color.WHITE);
		return(rowView);
	}
}
