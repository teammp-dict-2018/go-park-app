package com.goparkapp;

import android.os.Bundle;
import android.widget.LinearLayout;;

public class TermsOfService extends NavigationDrawer
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.terms_of_service_layout);
		LinearLayout mainLayout = (LinearLayout) findViewById(R.id.tos_layout);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
	}
}
