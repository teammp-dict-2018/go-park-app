package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.EditText;
import android.widget.LinearLayout;

class CardNumberField
{
	public EditText cardNumberField(Context c) {
		LinearLayout.LayoutParams forCardFieldWidth = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		GradientDrawable cardBorder = new GradientDrawable();
		cardBorder.setStroke(2, Color.RED);
		EditText cardField = new EditText(c);
		cardField.setBackground(cardBorder);
		cardField.setBackgroundColor(Color.WHITE);
		cardField.setLayoutParams(forCardFieldWidth);
		cardField.setTextColor(Color.parseColor("#3FB0AC"));
		return(cardField);
	}
}
