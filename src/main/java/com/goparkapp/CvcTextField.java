package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.EditText;
import android.widget.LinearLayout;

class CvcTextField
{
	public EditText cvcTextField(Context c) {
		LinearLayout.LayoutParams forCvcTextFieldLayout = new LinearLayout.LayoutParams(90, LinearLayout.LayoutParams.WRAP_CONTENT);
		GradientDrawable gd = new GradientDrawable();
		gd.setStroke(2, Color.parseColor("#3FB0AC"));
		EditText cvcField = new EditText(c);
		cvcField.setTextColor(Color.parseColor("#3FB0AC"));
		cvcField.setBackground(gd);
		cvcField.setBackgroundColor(Color.WHITE);
		cvcField.setLayoutParams(forCvcTextFieldLayout);
		return(cvcField);
	}
}
