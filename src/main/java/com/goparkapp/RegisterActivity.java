package com.goparkapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import android.widget.EditText;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;
import android.widget.LinearLayout;
import android.widget.Button;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.InputType;
import android.graphics.Typeface;
public class RegisterActivity extends AppCompatActivity {

	private final String TAG = "RegisterActivity";
	private FirebaseAuth mAuth;
	private EditText usernameField, emailField, passwordField;
	private Button registerBtn;
	private ImageView registerLogo;
	private TextView registerLbl, registerLoginLbl;
	private Typeface typeface;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		LinearLayout registerLayout = new LinearLayout(this);
		registerLayout.setOrientation(LinearLayout.VERTICAL);
		registerLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		registerLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		LinearLayout.LayoutParams logoParams = new LinearLayout.LayoutParams(300, 90);
		logoParams.setMargins(0, 100, 0, 100);
		LinearLayout.LayoutParams inputParams = new LinearLayout.LayoutParams(480, 80);
		inputParams.setMargins(0, 0, 0, 5);
		LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(400, 100);
		btnParams.setMargins(0, 50, 0, 50);
		registerLogo = new ImageView(this);
		registerLbl = new TextView(this);
		usernameField = new EditText(this);
		emailField = new EditText(this);
		passwordField = new EditText(this);
		registerBtn = new Button(this);
		registerLoginLbl = new TextView(this);
		registerLogo.setLayoutParams(logoParams);
		registerLogo.setImageResource(R.drawable.gopark_logo);
		registerLbl.setText("REGISTER");
		registerLbl.setTextColor(Color.WHITE);
		registerLbl.setGravity(Gravity.CENTER);
		registerLbl.setTypeface(null, Typeface.BOLD);
		registerLbl.setTextSize(20);
		registerLbl.setTypeface(typeface);
		registerLbl.setPadding(0, 0, 0, 100);
		emailField.setLayoutParams(inputParams);
		emailField.setHint("Email Address");
		emailField.setTextSize(14);
		emailField.setTypeface(typeface);
		emailField.setTextColor(Color.parseColor("#3FB0AC"));
		emailField.setHintTextColor(Color.parseColor("#3FB0AC"));
		emailField.setBackgroundColor(Color.WHITE);
		emailField.setPadding(5, 0, 0, 0);
		emailField.setCompoundDrawablesWithIntrinsicBounds(R.drawable.aticon, 0, 0, 0);
		passwordField.setLayoutParams(inputParams);
		passwordField.setHint("Password");
		passwordField.setTextColor(Color.parseColor("#3FB0AC"));
		passwordField.setTypeface(typeface);
		passwordField.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		passwordField.setMaxLines(1);
		passwordField.setSingleLine(false);
		passwordField.setTextSize(14);
		passwordField.setHintTextColor(Color.parseColor("#3FB0AC"));
		passwordField.setBackgroundColor(Color.WHITE);
		passwordField.setPadding(5, 0, 0, 0);
		passwordField.setCompoundDrawablesWithIntrinsicBounds(R.drawable.lock, 0, 0, 0);
		registerBtn.setLayoutParams(btnParams);
		registerBtn.setText("REGISTER");
		registerBtn.setTypeface(typeface);
		registerBtn.setTextSize(14);
		registerBtn.setTextColor(Color.WHITE);
		registerBtn.setBackgroundColor(Color.parseColor("#1B7A86"));
		registerLoginLbl.setText("Already have an account? Login");
		registerLoginLbl.setTextColor(Color.WHITE);
		registerLoginLbl.setGravity(Gravity.CENTER);
		registerLoginLbl.setTextSize(14);
		registerLoginLbl.setTypeface(typeface);
		registerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String email = String.valueOf(emailField.getText());
				String password = String.valueOf(passwordField.getText());
				mAuth = FirebaseAuth.getInstance();
				mAuth.createUserWithEmailAndPassword(email, password)
					.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if(emailField.getText().toString().length() == 0) {
								emailField.setError("Email Address field is required");
							}
							else if(passwordField.getText().toString().length() == 0) {
								passwordField.setError("Password is required");
							}
							else {
								if (task.isSuccessful()) {
								Log.d(TAG, "createUserWithEmail:success");
								FirebaseUser user = mAuth.getCurrentUser();
								updateUI(user);
								} 
								else {
									Log.w(TAG, "createUserWithEmail:failure", task.getException());
									Toast.makeText(RegisterActivity.this, "Authentication failed.",
											Toast.LENGTH_SHORT).show();
									updateUI(null);
								}
							}
						}
					});
			}
		});
		registerLoginLbl.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
			}
		});
		registerLayout.addView(registerLogo);
		registerLayout.addView(registerLbl);
		registerLayout.addView(emailField);
		registerLayout.addView(passwordField);
		registerLayout.addView(registerBtn);
		registerLayout.addView(registerLoginLbl);
		setContentView(registerLayout);
	}

	public void updateUI(FirebaseUser user) {
		if (user != null) {
			startActivity(new Intent(RegisterActivity.this, HomeScreenActivity.class));
		}
		else {
			return;
		}
	}
}
