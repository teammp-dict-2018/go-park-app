package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

class Checking
{
	public void checking(final Context c, final EditText forgotPasswordField, final Button changeButton, final Popup popup, final CheckingSigns signs) {
		final String specialCharacters = "([~`!#$%^&*=+\\/{}\'\":;?/><,|])";
		forgotPasswordField.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				final String email = forgotPasswordField.getText().toString();
				if(email.equals("")) {
					changeButton.setBackgroundColor(Color.parseColor("#919191"));
				}
				else {
					changeButton.setBackgroundColor(Color.parseColor("#1B7A86"));
					changeButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							if(email.matches(specialCharacters)) {
								popup.maximumCharPopup("Invalid character");
							}
							if(!email.contains("@") || !email.contains(".")) {
								if(email.length() > 50) {
									popup.maximumCharPopup("You've reached the maximum numbers of characters allowed.");
								}
								else {
									popup.maximumCharPopup("Please enter a valid email address.");
								}
							}
							else {
								if(email.contains("@")) {
									signs.checkingSigns(email, popup);
								}
								else if(email.contains(".")) {
									if(email.endsWith(".")) {
										popup.maximumCharPopup("Please enter a valid email address.");
									}
									else {
										signs.checkingSigns(email, popup);
									}
								}
							}	
						}
					});
				}
			}
		});
	}
}
