package com.goparkapp;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.InputType;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;
import android.widget.AdapterView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import java.util.Calendar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class TransactionsActivity extends NavigationDrawer
{
	private Typeface fs;
	private ListView lv;
	private TransactionsAdapter ta;
	private Typeface typeface;
	private RelativeLayout copyright;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.transactions_layout);
		LinearLayout tranLayout = (LinearLayout) findViewById(R.id.transactions_layout);
		tranLayout.setOrientation(LinearLayout.VERTICAL);
		tranLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		tranLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		tranLayout.addView(transactions());
		tranLayout.addView(copyright());
	}

	public LinearLayout transactions() {
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		LinearLayout rbLayout = new LinearLayout(this);
		rbLayout.setOrientation(LinearLayout.VERTICAL);
		TextView mn = new TextView(this);
		mn.setBackgroundColor(Color.WHITE);
		mn.setTextColor(Color.parseColor("#3FB0AC"));
		mn.setText("Mall Name");
		String mallN = mn.getText().toString();
		TextView d = new TextView(this);
		d.setBackgroundColor(Color.WHITE);
		d.setTextColor(Color.parseColor("#3FB0AC"));
		d.setText("Date");
		String dt = d.getText().toString();
		TextView tId = new TextView(this);
		tId.setBackgroundColor(Color.WHITE);
		tId.setTextColor(Color.parseColor("#3FB0AC"));
		tId.setText("Transaction ID");
		String ti = tId.getText().toString();
		String[] mallNames = { mallN, "MOA", "MegaMall", "Ayala Mall", "Nuvali" };
		String[] date = { dt, "08/09/2018", "08/09/2018", "08/09/2018", "08/09/2018", "08/09/2018" };
		String[] transactId = { ti, "WJ8EFY8E6H", "W50LHPHKT0", "POSOE9OG92", "SO4FJR43LH", "LD03IFSQWE" };
		ta = new TransactionsAdapter(this, mallNames, date, transactId);
		lv = new ListView(this);
		lv.setAdapter(ta);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String item = (String) lv.getItemAtPosition(position);
				Toast.makeText(getApplicationContext(), item, Toast.LENGTH_SHORT).show();
			}
		});
		rbLayout.addView(lv);
		return(rbLayout);
	}

	public RelativeLayout copyright() {
		copyright = new RelativeLayout(this);
		TextView cp = new TextView(this);
		cp.setText("2018 GoPark Philippines. All rights reserved.");
		cp.setTextSize(12);
		cp.setTextColor(Color.WHITE);
		cp.setPadding(0, 0, 0, 10);
		copyright.addView(cp);
		LayoutParams copyrightParams = (LayoutParams)cp.getLayoutParams();
		copyrightParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		copyrightParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		return(copyright);
	}
}

