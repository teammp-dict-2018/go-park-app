package com.goparkapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.content.Intent;
import android.view.Gravity;
import android.widget.Button;
import android.view.View;
import android.graphics.Color;
public class Receipt extends Activity
{
	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		LinearLayout l = new LinearLayout(this);
		l.setOrientation(LinearLayout.VERTICAL);
		l.setGravity(Gravity.CENTER);
		LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(480, 100);
		btnParams.setMargins(0, 50, 0, 0);
		TextView sample = new TextView(this);
		sample.setText("This is a sample receipt screen");
		sample.setGravity(Gravity.CENTER);
		Button btn = new Button(this);
		btn.setLayoutParams(btnParams);
		btn.setText("DONE");
		btn.setTextColor(Color.WHITE);
		btn.setBackgroundColor(Color.parseColor("#1B7A86"));
		btn.setGravity(Gravity.CENTER);
		btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(Receipt.this, CurrentBookingActivity.class));
			}
		});
		l.addView(sample);
		l.addView(btn);
		setContentView(l);
	}
}
