package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import java.util.ArrayList;

class ExpirationFieldMonth
{
	public LinearLayout monthDropDown(Context c) {
		DisplayMetrics width = c.getResources().getDisplayMetrics();
		double screenWidth = width.widthPixels*0.3;
		double screenWidthForSpinner = width.widthPixels*0.2;
		LinearLayout forSpinner = new LinearLayout(c);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams forSpinnerLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		forSpinner.setLayoutParams(lp);
		String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		ArrayList<String> month = new ArrayList<String>();
		for(int i=0; i<months.length; i++) {
			month.add(months[i]);
		}
		Spinner monthChoices = new Spinner(c, Spinner.MODE_DROPDOWN);
		monthChoices.setDropDownWidth((int)screenWidthForSpinner);
		monthChoices.setPopupBackgroundResource(R.drawable.spinnerbackground);
		ArrayAdapter<String> monthsChoices = new ArrayAdapter<String>(c, android.R.layout.simple_spinner_item, month);
		monthChoices.setAdapter(monthsChoices);
		monthsChoices.setDropDownViewResource(android.R.layout.simple_spinner_item);
		monthChoices.setLayoutParams(forSpinnerLayout);
		forSpinner.addView(monthChoices);
		return(forSpinner);
	}
}
