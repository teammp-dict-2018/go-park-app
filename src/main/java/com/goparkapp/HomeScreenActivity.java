package com.goparkapp;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.InputType;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;
import android.widget.AdapterView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import java.util.Calendar;

public class HomeScreenActivity extends NavigationDrawer
{
	private Button findAvailability;
	private Calendar cal;
	private DialogFragment stf;
	private EditText entryTimeInput;
	private EditText exitTimeInput;
	private EditText searchInput;
	private FragmentManager fm;
	private LinearLayout homeLayout;
	private LinearLayout timePickerLayout;
	private LinearLayout listLayout;
	private ListView lv;
	private MallListAdapter mla;
	private Typeface primaryFont;
	private Typeface secondaryFont;
	private TextView slogan;
	private int hour;
	private int minute;
	private int ampm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.home_layout);
		fm = getSupportFragmentManager();
		cal = Calendar.getInstance();
		hour = cal.get(Calendar.HOUR_OF_DAY);
		minute = cal.get(Calendar.MINUTE);
		ampm = cal.get(Calendar.AM_PM);
		homeLayout = (LinearLayout) findViewById(R.id.home_layout);
		homeLayout.setOrientation(LinearLayout.VERTICAL);
		homeLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		timePickerLayout = new LinearLayout(this);
		timePickerLayout.setOrientation(LinearLayout.VERTICAL);
		primaryFont = Typeface.createFromAsset(getAssets(), "font/OpenSans.ttf");
		secondaryFont = Typeface.createFromAsset(getAssets(), "font/montserrat.ttf");
		searchInput = (EditText) findViewById(R.id.search);
		searchInput.setHint("Search");
		searchInput.setHintTextColor(Color.parseColor("#3FB0AC"));
		searchInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.search, 0);
		searchInput.setTypeface(primaryFont);
		searchInput.setPadding(10, 0, 0, 0);
		LinearLayout.LayoutParams sloganParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		sloganParams.setMargins(20, 20, 20, 40);
		sloganParams.gravity = Gravity.CENTER;
		slogan = new TextView(this);
		slogan.setLayoutParams(sloganParams);
		slogan.setText("We find you space to save time");
		slogan.setTypeface(secondaryFont);
		slogan.setGravity(Gravity.CENTER);
		slogan.setTextColor(Color.WHITE);
		LinearLayout.LayoutParams timeParams = new LinearLayout.LayoutParams(600, LinearLayout.LayoutParams.WRAP_CONTENT);
		timeParams.setMargins(0, 20,0, 0);
		timeParams.gravity = Gravity.CENTER;
		entryTimeInput = new EditText(this);
		entryTimeInput.setLayoutParams(timeParams);
		entryTimeInput.setGravity(Gravity.CENTER);
		entryTimeInput.setTextColor(Color.parseColor("#3FB0AC"));
		entryTimeInput.setText(hour + " : " + minute);
		entryTimeInput.setCompoundDrawablesWithIntrinsicBounds(R.drawable.entry, 0, R.drawable.clock, 0);
		entryTimeInput.setInputType(InputType.TYPE_NULL);
		entryTimeInput.setTypeface(primaryFont);
		entryTimeInput.setBackgroundColor(Color.WHITE);
		entryTimeInput.setPadding(10, 10, 10, 10);
		exitTimeInput = new EditText(this);
		exitTimeInput.setLayoutParams(timeParams);
		exitTimeInput.setText((hour + 1) + " : " + minute);
		exitTimeInput.setGravity(Gravity.CENTER);
		exitTimeInput.setTextColor(Color.parseColor("#3FB0AC"));
		exitTimeInput.setCompoundDrawablesWithIntrinsicBounds(R.drawable.exit, 0, R.drawable.clock, 0);
		exitTimeInput.setInputType(InputType.TYPE_NULL);
		exitTimeInput.setTypeface(primaryFont);
		exitTimeInput.setBackgroundColor(Color.WHITE);
		exitTimeInput.setPadding(10, 10, 10, 10);
		entryTimeInput.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stf = new SetTimeFragment(entryTimeInput);
				stf.show(fm.beginTransaction(), "time picker");
			}
		});
		entryTimeInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean onFocus) {
				if (onFocus) {
					stf = new SetTimeFragment(entryTimeInput);
					stf.show(fm.beginTransaction(), "time picker");
				}
			}
		});
		exitTimeInput.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stf = new SetTimeFragment(exitTimeInput);
				stf.show(fm.beginTransaction(), "time picker");
			}
		});
		exitTimeInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean onFocus) {
				if (onFocus) {
					stf = new SetTimeFragment(exitTimeInput);
					stf.show(fm, "time picker");
				}
			}
		});
		homeLayout.addView(slogan);
		homeLayout.addView(timePickerLayout);
		timePickerLayout.addView(entryTimeInput);
		timePickerLayout.addView(exitTimeInput);
		LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(400, 90);
		btnParams.gravity = Gravity.CENTER;
		btnParams.setMargins(0, 40, 0, 40);
		findAvailability = new Button(this);
		findAvailability.setLayoutParams(btnParams);
		findAvailability.setText("FIND AVAILABILITY");
		findAvailability.setTextColor(Color.WHITE);
		findAvailability.setBackgroundColor(Color.parseColor("#1B7A86"));
		findAvailability.setTypeface(primaryFont);
		findAvailability.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Mall List has been updated.", Toast.LENGTH_SHORT).show();
			}
		}
	);
		homeLayout.addView(findAvailability);
		homeLayout.addView(mallParkingList());
	}

	public LinearLayout mallParkingList() {
		LinearLayout mallListLayout = new LinearLayout(this);
		mallListLayout.setOrientation(LinearLayout.VERTICAL);
		TextView mn = new TextView(this);
		mn.setTextColor(Color.parseColor("#3FB0AC"));
		mn.setText("Mall Name");
		String mallN = mn.getText().toString();
		TextView availableTime = new TextView(this);
		availableTime.setTextColor(Color.parseColor("#3FB0AC"));
		availableTime.setText("Availability");
		String ti = availableTime.getText().toString();
		String[] mallNames = { mallN, "MOA", "MegaMall", "Ayala Mall", "Nuvali" };
		String[] time = { ti, "09:00 AM - 12:00 PM", "09:00 AM - 2:00 PM", "10:00 AM - 12:00 PM", "01:00 PM - 2:00 PM", "03:00 PM - 05:00 PM" };
		mla = new MallListAdapter(this, mallNames, time);
		lv = new ListView(this);
		lv.setAdapter(mla);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String item = (String) lv.getItemAtPosition(position);
				Toast.makeText(getApplicationContext(), item, Toast.LENGTH_SHORT).show();
				startActivity(new Intent(getApplicationContext(), ParkingSlotActivity.class));
			}
		});
		mallListLayout.addView(mn);
		mallListLayout.addView(lv);
		return(mallListLayout);
	}

}
