package com.goparkapp;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.InputType;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;
import android.widget.AdapterView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import java.util.Calendar;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class AboutActivity extends NavigationDrawer
{
	private ImageView aboutLogo;
	private TextView aboutText, aboutCopyright;
	private Typeface fs;
	private RelativeLayout copyright;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.about_layout);
		LinearLayout aboutLayout = (LinearLayout) findViewById(R.id.about_layout);
		aboutLayout.setOrientation(LinearLayout.VERTICAL);
		aboutLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		aboutLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		LinearLayout.LayoutParams logoParams = new LinearLayout.LayoutParams(300, 90);
		logoParams.setMargins(0, 100, 0, 100);
		fs = Typeface.createFromAsset(getAssets(), "font/OpenSans.ttf");
		aboutLogo = new ImageView(this);
		aboutText = new TextView(this);
		aboutLogo.setLayoutParams(logoParams);
		aboutLogo.setImageResource(R.drawable.gopark_logo);
		aboutText.setText("Are you tired in finding an available parking space? By using this mobile application, you can save time and effort by easily searching an open parking space in malls. It is a free and easy to use application. A convenient parking space tracker right at your fingertips.");
		aboutText.setTextSize(14);
		aboutText.setTypeface(fs);
		aboutText.setPadding(30, 0, 30, 100);
		aboutText.setGravity(Gravity.CENTER);
		aboutText.setTypeface(null, Typeface.NORMAL);
		aboutText.setTextColor(Color.WHITE);
		aboutLayout.addView(aboutLogo);
		aboutLayout.addView(aboutText);
		aboutLayout.addView(copyright());
	}

	public RelativeLayout copyright() {
		copyright = new RelativeLayout(this);
		TextView cp = new TextView(this);
		cp.setText("2018 GoPark Philippines. All rights reserved.");
		cp.setTextSize(12);
		cp.setTextColor(Color.WHITE);
		cp.setPadding(0, 0, 0, 10);
		copyright.addView(cp);
		LayoutParams copyrightParams = (LayoutParams)cp.getLayoutParams();
		copyrightParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		copyrightParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		return(copyright);
	}
}
