package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TextView;

class ForgotPasswordLabel
{
	public TextView forgotPasswordLabel(Context c, Typeface tf) {
		TextView passwordLabel = new TextView(c);
		passwordLabel.setText("FORGOT PASSWORD");
		passwordLabel.setTypeface(tf, Typeface.BOLD);
		passwordLabel.setTextSize(20);
		passwordLabel.setTextColor(Color.WHITE);
		return(passwordLabel);
	}
}
