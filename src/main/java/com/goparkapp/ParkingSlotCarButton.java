package com.goparkapp;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ImageButton;
import android.widget.TextView;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class ParkingSlotCarButton
{
	Context ctx;
	Typeface typeface;

	public ParkingSlotCarButton(Context c) {
		ctx = c;
	}

	public LinearLayout upperSlot() {
		LinearLayout mainLayout = new LinearLayout(ctx);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout upperText = new LinearLayout(ctx);
		upperText.setOrientation(LinearLayout.HORIZONTAL);
		TextView firstRow = new TextView(ctx);
		firstRow.setText("S1");
		firstRow.setPadding(36, 0, 26, 0);
		textstyle(firstRow);
		TextView secondRow = new TextView(ctx);
		secondRow.setText("S3");
		secondRow.setPadding(30, 0, 23, 0);
		textstyle(secondRow);
		TextView thirdRow = new TextView(ctx);
		thirdRow.setText("S5");
		thirdRow.setPadding(31, 0, 24, 0);
		textstyle(thirdRow);
		TextView fourthRow = new TextView(ctx);
		fourthRow.setText("S7");
		fourthRow.setPadding(31, 0, 24, 0);
		textstyle(fourthRow);
		TextView fifthRow = new TextView(ctx);
		fifthRow.setText("S9");
		fifthRow.setPadding(32, 0, 26, 0);
		textstyle(fifthRow);
		TextView sixthRow = new TextView(ctx);
		sixthRow.setText("S11");
		sixthRow.setPadding(26, 0, 25, 0);
		textstyle(sixthRow);
		upperText.addView(firstRow);
		upperText.addView(secondRow);
		upperText.addView(thirdRow);
		upperText.addView(fourthRow);
		upperText.addView(fifthRow);
		upperText.addView(sixthRow);
		upperText.setPadding(0, 40, 0, 10);
		upperText.setGravity(Gravity.CENTER);
		LinearLayout upperSlot = new LinearLayout(ctx);
		upperSlot.setOrientation(LinearLayout.HORIZONTAL);
		ImageButton bluecar1 = new ImageButton(ctx);
		bluecar1.setImageResource(R.drawable.bluecar);
		imgBtn(bluecar1);
		ImageButton whitecar1 = new ImageButton(ctx);
		whitecar1.setImageResource(R.drawable.whitecar);
		imgBtn(whitecar1);
		ImageButton bluecar2 = new ImageButton(ctx);
		bluecar2.setImageResource(R.drawable.bluecar);
		imgBtn(bluecar2);
		ImageButton whitecar2 = new ImageButton(ctx);
		whitecar2.setImageResource(R.drawable.whitecar);
		imgBtn(whitecar2);
		ImageButton bluecar3 = new ImageButton(ctx);
		bluecar3.setImageResource(R.drawable.bluecar);
		imgBtn(bluecar3);
		ImageButton whitecar3 = new ImageButton(ctx);
		whitecar3.setImageResource(R.drawable.whitecar);
		imgBtn(whitecar3);
		upperSlot.addView(bluecar1);
		upperSlot.addView(bluecar2);
		upperSlot.addView(whitecar1);
		upperSlot.addView(bluecar3);
		upperSlot.addView(whitecar2);
		upperSlot.addView(whitecar3);;
		upperSlot.setGravity(Gravity.CENTER);
		mainLayout.addView(upperText);
		mainLayout.addView(upperSlot);
		return(mainLayout);
	}

	public LinearLayout bottomSlot() {
		LinearLayout mainLayout = new LinearLayout(ctx);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout bottomText = new LinearLayout(ctx);
		bottomText.setOrientation(LinearLayout.HORIZONTAL);
		TextView firstRow = new TextView(ctx);
		firstRow.setText("S2");
		firstRow.setPadding(35, 0, 24, 0);
		textstyle(firstRow);
		TextView secondRow = new TextView(ctx);
		secondRow.setText("S4");
		secondRow.setPadding(34, 0, 17, 0);
		textstyle(secondRow);
		TextView thirdRow = new TextView(ctx);
		thirdRow.setText("S6");
		thirdRow.setPadding(38, 0, 19, 0);
		textstyle(thirdRow);
		TextView fourthRow = new TextView(ctx);
		fourthRow.setText("S8");
		fourthRow.setPadding(36, 0, 19, 0);
		textstyle(fourthRow);
		TextView fifthRow = new TextView(ctx);
		fifthRow.setText("S10");
		fifthRow.setPadding(30, 0, 19, 0);
		textstyle(fifthRow);
		TextView sixthRow = new TextView(ctx);
		sixthRow.setText("S12");
		sixthRow.setPadding(23, 0, 27, 0);
		textstyle(sixthRow);
		bottomText.addView(firstRow);
		bottomText.addView(secondRow);
		bottomText.addView(thirdRow);
		bottomText.addView(fourthRow);
		bottomText.addView(fifthRow);
		bottomText.addView(sixthRow);
		bottomText.setPadding(0, 0, 0, 10);
		bottomText.setGravity(Gravity.CENTER);
		LinearLayout bottomSlot = new LinearLayout(ctx);
		bottomSlot.setOrientation(LinearLayout.HORIZONTAL);
		ImageButton bluecar1 = new ImageButton(ctx);
		bluecar1.setImageResource(R.drawable.bluecar);
		imgBtn(bluecar1);
		ImageButton whitecar1 = new ImageButton(ctx);
		whitecar1.setImageResource(R.drawable.whitecar);
		imgBtn(whitecar1);
		ImageButton bluecar2 = new ImageButton(ctx);
		bluecar2.setImageResource(R.drawable.bluecar);
		imgBtn(bluecar2);
		ImageButton whitecar2 = new ImageButton(ctx);
		whitecar2.setImageResource(R.drawable.whitecar);
		imgBtn(whitecar2);
		ImageButton bluecar3 = new ImageButton(ctx);
		bluecar3.setImageResource(R.drawable.bluecar);
		imgBtn(bluecar3);
		ImageButton whitecar3 = new ImageButton(ctx);
		whitecar3.setImageResource(R.drawable.whitecar);
		imgBtn(whitecar3);
		bottomSlot.addView(whitecar1);
		bottomSlot.addView(bluecar1);
		bottomSlot.addView(bluecar2);
		bottomSlot.addView(whitecar2);
		bottomSlot.addView(whitecar3);
		bottomSlot.addView(bluecar3);
		bottomSlot.setGravity(Gravity.CENTER);
		bottomSlot.setPadding(0, 220, 0, 0);
		mainLayout.addView(bottomSlot);
		mainLayout.addView(bottomText);
		return(mainLayout);
	}

	public ImageButton imgBtn(ImageButton ib) {
		ib.setPadding(20, 0, 20, 0);
		ib.setBackgroundResource(0);
		ib.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageView iv = new ImageView(ctx);
				iv.setImageResource(R.drawable.check);
			}
		});
		return(ib);
	}

	public TextView textstyle(TextView tv) {
		typeface = Typeface.createFromAsset(ctx.getAssets(),"font/OpenSans.ttf");
		tv.setTypeface(typeface);
		tv.setTextColor(Color.WHITE);
		tv.setPaintFlags(tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		return(tv);
	}

}
