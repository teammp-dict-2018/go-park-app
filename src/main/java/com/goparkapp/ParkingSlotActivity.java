package com.goparkapp;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.Typeface;
import android.graphics.Color;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.Gravity;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.FrameLayout;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.util.DisplayMetrics;
import android.content.Intent;
import android.text.InputFilter;
import java.util.ArrayList;

public class ParkingSlotActivity extends Activity
{
	ParkingSlot ps;
	ParkingSlot currentlySelected;
	ParkingSlotTextStyle psts;
	ArrayList<ParkingSlot> parkingSlots;
	private ArrayList<String> floors;
	private int[] floor1state = {0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1};
	private int[] floor2state = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1};
	private int[] floor3state = {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1};
	private int[] floor4state = {1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1};
	private int[] floor5state = {0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1};
	private Spinner selectFloor;
	private ArrayAdapter<String> adapter;
	private DisplayMetrics displayMetrics;
	private Display display;
	private Point size;
	private Typeface typeface;
	private RelativeLayout headerLayout;
	private RelativeLayout bknwButton;
	private RelativeLayout copyright;
	private LinearLayout mainLayout;
	private LinearLayout lgndlayout;
	private LinearLayout legendslayout;
	private LinearLayout firstlayout;
	private LinearLayout secondlayout;
	private LinearLayout thirdlayout;
	private LinearLayout mallDtls;
	private LinearLayout spinnerlayout;
	private ImageButton searchBtn;
	private ImageButton backBtn;
	private ImageView pslot;
	private Button booknow;
	private double dpWidth;
	private TextView addrss1;
	private TextView addrss2;
	private TextView distance;
	private TextView time;
	private TextView mallName;
	private EditText srchBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		displayMetrics = getResources().getDisplayMetrics();
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		mainLayout.addView(headers());
		mainLayout.addView(mallDetails());
		mainLayout.addView(mallFloors());
		mainLayout.addView(legends());
		mainLayout.addView(booknowBttn());
		mainLayout.addView(copyright());
		initializeParkingSlotStates(floor1state);
		setContentView(mainLayout);
		mainLayout.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
					if(srchBar.getVisibility() == View.VISIBLE) {
						srchBar.setText("");
						srchBar.setVisibility(View.GONE);
						mallName.setVisibility(View.VISIBLE);
					}
				}
				return true;
			}
		});
	}

	public RelativeLayout headers() {
		headerLayout = new RelativeLayout(this);
		backBtn = new ImageButton(this);
		searchBtn = new ImageButton(this);
		mallName = new TextView(this);
		srchBar = new EditText(this);
		srchBar.setWidth(450);
		srchBar.setPadding(10, 10, 10, 10);
		srchBar.setSingleLine();
		srchBar.setTextColor(Color.parseColor("#3FB0AC"));
		srchBar.setBackgroundColor(Color.WHITE);
		srchBar.setTextSize(14);
		srchBar.setHint("Search...");
		srchBar.setHintTextColor(Color.parseColor("#3FB0AC"));
		srchBar.setFilters(new InputFilter[]{new InputFilter.
			LengthFilter(50)});
		srchBar.setVisibility(View.GONE);
		mallName.setTypeface(typeface);
		mallName.setTypeface(null, Typeface.BOLD);
		mallName.setTextSize(20);
		mallName.setTextColor(Color.WHITE);
		mallName.setText("MALL A");
		backBtn.setImageResource(R.drawable.back);
		backBtn.setBackgroundResource(0);
		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
				startActivity(intent);
			}
		});
		searchBtn.setImageResource(R.drawable.whitesearch);
		searchBtn.setBackgroundResource(0);
		searchBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mallName.getVisibility() == View.VISIBLE) {
					srchBar.setText("");
					srchBar.setVisibility(View.VISIBLE);
					mallName.setVisibility(View.GONE);
				}
				else {
					srchBar.setText("");
					srchBar.setVisibility(View.GONE);
					mallName.setVisibility(View.VISIBLE);
				}
			}
		});
		headerLayout.addView(backBtn);
		headerLayout.addView(srchBar);
		headerLayout.addView(mallName);
		headerLayout.addView(searchBtn);
		LayoutParams backBtnParams = (LayoutParams)backBtn.getLayoutParams();
		backBtnParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams textParams = (LayoutParams)mallName.getLayoutParams();
		textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		textParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		LayoutParams srchBarParams = (LayoutParams)srchBar.getLayoutParams();
		srchBarParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		srchBarParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		LayoutParams srchBtnParams = (LayoutParams)searchBtn.getLayoutParams();
		srchBtnParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		headerLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		return(headerLayout);
	}

	public LinearLayout mallDetails() {
		psts = new ParkingSlotTextStyle(this);
		mallDtls = new LinearLayout(this);
		mallDtls.setOrientation(LinearLayout.VERTICAL);
		addrss1 = new TextView(this);
		psts.mainTextStyles(addrss1);
		addrss1.setText("Seaside Blvd, 123 MM");
		addrss2 = new TextView(this);
		psts.mainTextStyles(addrss2);
		addrss2.setText("Pasay, 1300 Metro Manila");
		distance = new TextView(this);
		psts.mainTextStyles(distance);
		distance.setText("Estimated distance: 3 kilometers");
		time = new TextView(this);
		psts.mainTextStyles(time);
		time.setText("Estimated time: 15 minutes");
		mallDtls.addView(addrss1);
		mallDtls.addView(addrss2);
		mallDtls.addView(distance);
		mallDtls.addView(time);
		mallDtls.setPadding(10, 20, 0, 10);
		return(mallDtls);
	}

	public LinearLayout mallFloors() {
		final LinearLayout mainfloorsLayout = new LinearLayout(this);
		mainfloorsLayout.setOrientation(LinearLayout.VERTICAL);
		mainfloorsLayout.setGravity(Gravity.CENTER);
		display = getWindowManager().getDefaultDisplay();
		size = new Point();
		try {
			display.getRealSize(size);
		} catch (NoSuchMethodError err) {
			display.getSize(size);
		}
		int width = size.x;
		int height = size.y;
		spinnerlayout = new LinearLayout(this);
		spinnerlayout.setBackgroundColor(Color.parseColor("#ffffff"));
		final FrameLayout parkingSlotsLayout = new FrameLayout(this);
		pslot = new ImageView(this);
		LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 500);
		pslot.setLayoutParams(imgParams);
		pslot.setImageResource(R.drawable.parking);
		final LinearLayout upperfloorLayout = new LinearLayout(this);
		upperfloorLayout.setGravity(Gravity.CENTER);
		upperfloorLayout.setOrientation(LinearLayout.HORIZONTAL);
		upperfloorLayout.setPadding(0, 150, 0, 0);
		LinearLayout upperText = new LinearLayout(this);
		upperText.setOrientation(LinearLayout.VERTICAL);
		final LinearLayout upperFloorRows = new LinearLayout(this);
		upperFloorRows.setOrientation(LinearLayout.HORIZONTAL);
		upperFloorRows.setGravity(Gravity.CENTER);
		upperFloorRows.setPadding(0, 80, 0, 10);
		final LinearLayout lowerfloorLayout = new LinearLayout(this);
		lowerfloorLayout.setGravity(Gravity.CENTER);
		lowerfloorLayout.setOrientation(LinearLayout.HORIZONTAL);
		lowerfloorLayout.setPadding(0, 0, 0, 190);
		LinearLayout lowerText = new LinearLayout(this);
		lowerText.setOrientation(LinearLayout.VERTICAL);
		final LinearLayout lowerFloorRows = new LinearLayout(this);
		lowerFloorRows.setOrientation(LinearLayout.HORIZONTAL);
		lowerFloorRows.setGravity(Gravity.CENTER);
		lowerFloorRows.setPadding(0, 370, 0, 10);
		floors = new ArrayList<String>();
		floors.add("Floor 1");
		floors.add("Floor 2");
		floors.add("Floor 3");
		floors.add("Floor 4");
		floors.add("Floor 5");
		selectFloor = new Spinner(this, Spinner.MODE_DROPDOWN);
		selectFloor.setPopupBackgroundResource(R.drawable.spinnerbackground);
		selectFloor.setDropDownWidth(197);
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, floors);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
		selectFloor.setAdapter(adapter);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)(width/3), LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.gravity = Gravity.CENTER;
		lp.setMargins(0, 20, 0, 0);
		spinnerlayout.setLayoutParams(lp);
		selectFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String selectedItemText = (String) parent.getItemAtPosition(position);
				if(selectedItemText.equals("Floor 1")) {
					upperFloorRows.removeAllViewsInLayout();
					lowerFloorRows.removeAllViewsInLayout();
					initializeParkingSlotStates(floor1state);
					upperFloorRows.addView(generateUpperSlots("R"));
					lowerFloorRows.addView(generateLowerSlots("R"));
				}
				else if(selectedItemText.equals("Floor 2")) {
					upperFloorRows.removeAllViewsInLayout();
					lowerFloorRows.removeAllViewsInLayout();
					initializeParkingSlotStates(floor2state);
					upperFloorRows.addView(generateUpperSlots("S"));
					lowerFloorRows.addView(generateLowerSlots("S"));
				}
				else if(selectedItemText.equals("Floor 3")) {
					upperFloorRows.removeAllViewsInLayout();
					lowerFloorRows.removeAllViewsInLayout();
					initializeParkingSlotStates(floor3state);
					upperFloorRows.addView(generateUpperSlots("T"));
					lowerFloorRows.addView(generateLowerSlots("T"));
				}
				else if(selectedItemText.equals("Floor 4")) {
					upperFloorRows.removeAllViewsInLayout();
					lowerFloorRows.removeAllViewsInLayout();
					initializeParkingSlotStates(floor4state);
					upperFloorRows.addView(generateUpperSlots("U"));
					lowerFloorRows.addView(generateLowerSlots("U"));
				}
				else if(selectedItemText.equals("Floor 5")) {
					upperFloorRows.removeAllViewsInLayout();
					lowerFloorRows.removeAllViewsInLayout();
					initializeParkingSlotStates(floor5state);
					upperFloorRows.addView(generateUpperSlots("V"));
					lowerFloorRows.addView(generateLowerSlots("V"));
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		parkingSlots = new ArrayList<ParkingSlot>();
		for(int i = 0; i < 12; i++) {
			ps = new ParkingSlot(this);
			ps.setIndex(i);
			ps.setClickHandler(new ParkingSlot.ParkingSlotClickListener() {
				public void onParkingSlotClick(int index, int state){
					onClick(index, state);
				}
			});
			parkingSlots.add(ps);
			if(i >= 6) {
				upperfloorLayout.addView(ps);
			}
			else if(i <= 7) {
				lowerfloorLayout.addView(ps);
			}
		}
		spinnerlayout.addView(selectFloor);
		upperText.addView(upperFloorRows);
		lowerText.addView(lowerFloorRows);
		parkingSlotsLayout.addView(pslot);
		parkingSlotsLayout.addView(upperText);
		parkingSlotsLayout.addView(upperfloorLayout);
		parkingSlotsLayout.addView(lowerfloorLayout);
		parkingSlotsLayout.addView(lowerText);
		mainfloorsLayout.addView(spinnerlayout);
		mainfloorsLayout.addView(parkingSlotsLayout);
		return(mainfloorsLayout);
	}

	public LinearLayout generateUpperSlots(String row) {
		LinearLayout textLayout = new LinearLayout(this);
		textLayout.setOrientation(LinearLayout.HORIZONTAL);
		TextView floor = null;
		for(int i = 1; i <= 6; i++) {
			floor = new TextView(this);
			floor.setText(row + i);
			psts.textstyle(floor);
			textLayout.addView(floor);
		}
		return(textLayout);
	}

	public LinearLayout generateLowerSlots(String row) {
		LinearLayout textLayout = new LinearLayout(this);
		textLayout.setOrientation(LinearLayout.HORIZONTAL);
		TextView floor = null;
		for(int i = 7; i <= 12; i++) {
			floor = new TextView(this);
			floor.setText(row + i);
			psts.lowerTextstyle(floor);
			textLayout.addView(floor);
		}
		return(textLayout);
	}

	public LinearLayout legends() {
		lgndlayout = new LinearLayout(this);
		lgndlayout.setOrientation(LinearLayout.HORIZONTAL);
		TextView legendLabel = new TextView(this);
		legendLabel.setText("Legend:   ");
		psts.mainTextStyles(legendLabel);
		legendLabel.setPadding(100, 0, 0, 0 );
		legendslayout = new LinearLayout(this);
		legendslayout.setOrientation(LinearLayout.VERTICAL);
		firstlayout = new LinearLayout(this);
		ImageView bluecar = new ImageView(this);
		bluecar.setImageResource(R.drawable.bluecar);
		TextView occLabel = new TextView(this);
		occLabel.setText("Occupied Space");
		layoutStyle(firstlayout, bluecar, occLabel);
		secondlayout = new LinearLayout(this);
		ImageView whitecar = new ImageView(this);
		whitecar.setImageResource(R.drawable.whitecar);
		TextView availLabel = new TextView(this);
		availLabel.setText("Available Space");
		layoutStyle(secondlayout, whitecar, availLabel);
		thirdlayout = new LinearLayout(this);
		ImageView check = new ImageView(this);
		check.setImageResource(R.drawable.check);
		TextView selectLabel = new TextView(this);
		selectLabel.setText("Selected Space");
		layoutStyle(thirdlayout, check, selectLabel);
		legendslayout.addView(firstlayout);
		legendslayout.addView(secondlayout);
		legendslayout.addView(thirdlayout);
		lgndlayout.addView(legendLabel);
		lgndlayout.addView(legendslayout);
		return(lgndlayout);
	}

	public RelativeLayout booknowBttn() {
		bknwButton = new RelativeLayout(this);
		dpWidth = displayMetrics.widthPixels;
		booknow = new Button(this);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)(dpWidth * 0.7) - 90, LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(0, 50, 0, 0);
		lp.gravity = Gravity.CENTER;
		booknow.setLayoutParams(lp);
		booknow.setBackgroundColor(Color.parseColor("#919191"));
		booknow.setText("BOOK NOW");
		booknow.setTextColor(Color.WHITE);
		booknow.setTextSize(14);
		bknwButton.addView(booknow);
		LayoutParams textParams = (LayoutParams)booknow.getLayoutParams();
		textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		textParams.addRule(RelativeLayout.ALIGN_BOTTOM);
		return(bknwButton);
	}

	public RelativeLayout copyright() {
		copyright = new RelativeLayout(this);
		TextView cp = new TextView(this);
		cp.setTypeface(typeface);
		cp.setText("2018 GoPark Philippines. All rights reserved.");
		cp.setTextSize(12);
		cp.setTextColor(Color.WHITE);
		cp.setPadding(0, 0, 0, 10);
		copyright.addView(cp);
		LayoutParams copyrightParams = (LayoutParams)cp.getLayoutParams();
		copyrightParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		copyrightParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		return(copyright);
	}

	public void initializeParkingSlotStates(int[] states) {
		for(int i = 0; i < 12; i++) {
			ParkingSlot p = parkingSlots.get(i);
			p.setState(states[i]);
		}
	}

	public void onClick(int index, int state) {
		if(state == ParkingSlot.OCCUPIED) {
			return;
		}
		if(state == ParkingSlot.AVAILABLE) {
			if(currentlySelected != null) {
				currentlySelected.setState(ParkingSlot.AVAILABLE);
			}
			booknow.setBackgroundColor(Color.parseColor("#1B7A86"));
			booknow.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent(getApplicationContext(), PaymentMethod.class);
					startActivity(intent);
				}
			});
			ParkingSlot ps = parkingSlots.get(index);
			ps.setState(ParkingSlot.SELECTED);
			currentlySelected = ps;
		}
	}


	public LinearLayout layoutStyle(LinearLayout ll, ImageView imgview, TextView tv){
		ll.setOrientation(LinearLayout.HORIZONTAL);
		ll.setGravity(Gravity.CENTER);
		ll.addView(imgview);
		ll.addView(tv);
		tv.setPadding(20, 0, 0, 0 );
		psts.mainTextStyles(tv);
		return(ll);
	}
}
