package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

class MasterCardPaymentMethod
{
	Context c;

	public MasterCardPaymentMethod(Context c) {
		this.c = c;
	}

	public ImageView masterCardImage() {
		ImageView mcImage = new ImageView(c);
		mcImage.setImageResource(R.drawable.mastercard);
		LinearLayout.LayoutParams forMasterCard = new LinearLayout.LayoutParams(90, 90);
		mcImage.setLayoutParams(forMasterCard);
		return(mcImage);
	}

	public ImageView masterCardClickEvent(ImageView masterCard, final ImageView visa, final ImageView paypal, final GradientDrawable gd, final GradientDrawable gd2, final GradientDrawable gd3) {
		final ImageView mcImage = masterCard;
		mcImage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				gd2.setStroke(2, Color. WHITE);
				mcImage.setPadding(10, -300, 10, -300);
				gd.setStroke(0, Color. WHITE);
				visa.setPadding(0, 0, 0, 0);
				gd3.setStroke(0, Color. WHITE);
				paypal.setPadding(0, 0, 0, 0);
				mcImage.setBackground(gd2);
			}
		});
		return(mcImage);
	}
}
