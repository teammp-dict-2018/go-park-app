package com.goparkapp;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.Gravity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;

public class LoginActivity extends Activity
{
	private final String TAG = "LoginActivity";
	private FirebaseAuth mAuth;
	private ImageView logo;
	private EditText email;
	private EditText pw;
	private Button loginBtn;
	private TextView loginLbl;
	private TextView forgotPw;
	private TextView noAccount;
	private TextView registerLink;
	private Typeface typeface;

	@Override
	public void onStart() {
		super.onStart();
		FirebaseUser currentUser = mAuth.getCurrentUser();
		updateUI(currentUser);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		mAuth = FirebaseAuth.getInstance();
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		LinearLayout loginLayout = (LinearLayout) findViewById(R.id.login_layout);
		loginLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		LinearLayout.LayoutParams logoParams = new LinearLayout.LayoutParams(300, 90);
		logoParams.setMargins(0, 100, 0, 100);
		LinearLayout.LayoutParams inputParams = new LinearLayout.LayoutParams(480, 80);
		inputParams.setMargins(0, 0, 0, 5);
		LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(400, 100);
		btnParams.setMargins(10, 0, 0, 50);
		logo = new ImageView(this);
		logo.setLayoutParams(logoParams);
		logo.setImageResource(R.drawable.gopark_logo);
		loginLbl = new TextView(this);
		email = new EditText(this);
		pw = new EditText(this);
		forgotPw = new TextView(this);
		loginBtn = new Button(this);
		noAccount = new TextView(this);
		registerLink = new TextView(this);
		loginLbl.setText("LOGIN");
		loginLbl.setTextColor(Color.WHITE);
		loginLbl.setGravity(Gravity.CENTER);
		loginLbl.setTypeface(null, Typeface.BOLD);
		loginLbl.setTextSize(20);
		loginLbl.setTypeface(typeface);
		loginLbl.setPadding(0, 0, 0, 100);
		email.setLayoutParams(inputParams);
		email.setHint("Email Address");
		email.setTextSize(14);
		email.setTypeface(typeface);
		email.setTextColor(Color.parseColor("#3FB0AC"));
		email.setHintTextColor(Color.parseColor("#3FB0AC"));
		email.setBackgroundColor(Color.WHITE);
		email.setPadding(5, 0, 0, 0);
		email.setCompoundDrawablesWithIntrinsicBounds(R.drawable.aticon, 0, 0, 0);
		pw.setLayoutParams(inputParams);
		pw.setHint("Password");
		pw.setTextColor(Color.parseColor("#3FB0AC"));
		pw.setTypeface(typeface);
		pw.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		pw.setMaxLines(8);
		pw.setSingleLine(false);
		pw.setTextSize(14);
		pw.setHintTextColor(Color.parseColor("#3FB0AC"));
		pw.setBackgroundColor(Color.WHITE);
		pw.setPadding(5, 0, 0, 0);
		pw.setCompoundDrawablesWithIntrinsicBounds(R.drawable.lock, 0, 0, 0);
		forgotPw.setText("Forgot your password?");
		forgotPw.setTextSize(14);
		forgotPw.setTypeface(typeface);
		forgotPw.setTextColor(Color.WHITE);
		forgotPw.setGravity(Gravity.CENTER);
		forgotPw.setPadding(0, 0, 0, 50);
		loginBtn.setLayoutParams(btnParams);
		loginBtn.setText("LOGIN");
		loginBtn.setTypeface(typeface);
		loginBtn.setTextSize(14);
		loginBtn.setTextColor(Color.WHITE);
		loginBtn.setBackgroundColor(Color.parseColor("#1B7A86"));
		noAccount.setText("Don't have an account yet?");
		noAccount.setTextColor(Color.WHITE);
		noAccount.setTextSize(14);
		noAccount.setTypeface(typeface);
		noAccount.setGravity(Gravity.CENTER);
		registerLink.setText("Register");
		registerLink.setTextColor(Color.WHITE);
		registerLink.setGravity(Gravity.CENTER);
		registerLink.setTextSize(14);
		registerLink.setTypeface(typeface);
		loginLayout.addView(logo);
		loginLayout.addView(loginLbl);
		loginLayout.addView(email);
		loginLayout.addView(pw);
		loginLayout.addView(forgotPw);
		loginLayout.addView(loginBtn);
		loginLayout.addView(noAccount);
		loginLayout.addView(registerLink);
		loginBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String em = String.valueOf(email.getText());
				String pass = String.valueOf(pw.getText());
				mAuth.signInWithEmailAndPassword(em, pass)
						.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						if(email.getText().toString().length() == 0) {
							email.setError("Email Address field is required");
						}
						else if(pw.getText().toString().length() == 0) {
							pw.setError("Password is required");
						}
						else {
							if (task.isSuccessful()) {
								Log.d(TAG, "signInWithEmail:success");
								FirebaseUser user = mAuth.getCurrentUser();
								updateUI(user);
							} else {
								Log.w(TAG, "signInWithEmail:failure", task.getException());
								Toast.makeText(LoginActivity.this, "Authentication failed.",
										Toast.LENGTH_SHORT).show();
								updateUI(null);
							}
						}
					}
			});
			}
		});
		forgotPw.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
			}
		});
		registerLink.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
			}
		});
	}

	public void updateUI(FirebaseUser user) {
		if (user != null) {
			startActivity(new Intent(LoginActivity.this, HomeScreenActivity.class));
		}
		else {
			return;
		}
	}
}
