package com.goparkapp;

import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

class CvcAndCardHoldersNameTextField
{
	public LinearLayout allTextFields(Context c) {
		LinearLayout mainHolder = new LinearLayout(c);
		RelativeLayout fieldHolder = new RelativeLayout(c);
		CvcTextField cvc = new CvcTextField();
		CardHoldersNameTextField cardHoldersName = new CardHoldersNameTextField();
		EditText forCvc = cvc.cvcTextField(c);
		EditText cardHolder = cardHoldersName.cardHoldersNameTextField(c);
		fieldHolder.addView(forCvc);
		fieldHolder.addView(cardHolder);
		LayoutParams forCvcLayout = (LayoutParams)forCvc.getLayoutParams();
		forCvcLayout.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams forCardHolder = (LayoutParams)cardHolder.getLayoutParams();
		forCardHolder.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		fieldHolder.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		mainHolder.addView(fieldHolder);
		return(mainHolder);
	}
}
