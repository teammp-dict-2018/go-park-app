package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

class VisaPaymentMethod
{
	Context c;

	public VisaPaymentMethod(Context c) {
		this.c = c;
	}

	public ImageView visaImage() {
		ImageView visaImageView = new ImageView(c);
		visaImageView.setImageResource(R.drawable.visa);
		LinearLayout.LayoutParams forVisaImage = new LinearLayout.LayoutParams(90, 90);
		visaImageView.setLayoutParams(forVisaImage);
		return(visaImageView);
	}

	public ImageView visaClickEvent(ImageView visa, final ImageView masterCard, final ImageView paypal, final GradientDrawable gd, final GradientDrawable gd2, final GradientDrawable gd3, final LinearLayout mainLayout, final CardNumberLabel cnl, final CardNumberField cnf, final Typeface tf, final ExpirationDateLabel edl, final ExpirationFields ef, final CvcAndCardHoldersNameLabel cachnl, final CvcAndCardHoldersNameTextField textFields, final ConfirmPayment cp) {
		final ImageView visaImageView = visa;
		visaImageView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				gd.setStroke(2, Color.WHITE);
				visaImageView.setPadding(10, -300, 10, -300);
				gd2.setStroke(0, Color.WHITE);
				masterCard.setPadding(0, 0, 0, 0);
				gd3.setStroke(0, Color.WHITE);
				paypal.setPadding(0, 0, 0, 0);
				visaImageView.setBackground(gd);
				Limit limit = new Limit();
				mainLayout.addView(limit.limitAll(c, tf, cnl, cnf, edl, ef, cachnl, textFields, cp));
			}
		});
		return(visaImageView);
	}
}
