package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.TextView;

class PaymentMethodLabel
{
	Context c;

	public PaymentMethodLabel(Context c) {
		this.c = c;
	}

	public TextView paymentMethodLabel() {
		TextView paymentMethod = new TextView(c);
		Typeface tf = Typeface.createFromAsset(c.getAssets(), "font/OpenSans.ttf");
		paymentMethod.setText("PAYMENT METHODS");
		paymentMethod.setTypeface(tf);
		paymentMethod.setTextSize(20);
		paymentMethod.setTextColor(Color.WHITE);
		return(paymentMethod);
	}
}
