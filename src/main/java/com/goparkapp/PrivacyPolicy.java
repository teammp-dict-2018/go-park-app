package com.goparkapp;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.graphics.Typeface;
import android.widget.TextView;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.ScrollView;

public class PrivacyPolicy extends NavigationDrawer
{
	TextView pp;
	LinearLayout container;
	ScrollView sc;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.privacy_policy_layout);
		LinearLayout mainLayout = (LinearLayout) findViewById(R.id.pp_layout);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		Typeface fs = Typeface.createFromAsset(getAssets(), "font/OpenSans.ttf");
		mainLayout.addView(contentPrivacyPolicy(fs));
		mainLayout.addView(cpyrghtLabel(fs));
	}

	public LinearLayout contentPrivacyPolicy(Typeface fs) {
		container = new LinearLayout(this);
		container.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams c = new LinearLayout.LayoutParams(LinearLayout.
			LayoutParams.MATCH_PARENT, 0, 1);
		container.setLayoutParams(c);
		sc = new ScrollView(this);
		container.setPadding(50, 50, 50, 150);
		container.addView(sc);
		TextView con = new TextView(this);
		con.setText("By using GoPark mobile application, it understood"
			+ " that you trust us with your information. We valued it by committing"
			+ " ourselves in helping you understand our privacy policy. You should"
			+ " know what the information we collect and why we collect it.\n\n"
			+ " Kinds of information we collected:\n"
			+ " Username and password, Firstname, Lastname and Address,"
			+ " Type and Vehicle Plate #, Email-address, Phone number,"
			+ " Billing information.\n\n" + "Reasons for collecting information:\n"
			+ " We can also collect information from other sources"
			+ " like our payment providers and social media services.\n\n"
			+ " Security and Protection:\n" + "In compliance in Data Privacy Act"
			+ " of 2012 (Republic Act 10173), we make sure that your information"
			+ " is secured and protected against any alteration and disclosure,"
			+ " as well as any other unlawful processing");
		con.setTextSize(14);
		con.setTypeface(fs);
		con.setTypeface(null, Typeface.NORMAL);
		con.setPadding(30, 30, 30, 30);
		con.setTextColor(Color.WHITE);
		con.setGravity(Gravity.CENTER);
		sc.addView(con);
		return(container);
	}

	public LinearLayout cpyrghtLabel(Typeface fs) {
		container = new LinearLayout(this);
		container.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams copyright = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		copyright.gravity = Gravity.CENTER;
		container.setGravity(Gravity.BOTTOM);
		container.setLayoutParams(copyright);
		TextView cpyrght = new TextView(this);
		cpyrght.setText("2018 GoPark Philippines. All Rights Reserved");
		cpyrght.setTextSize(12);
		cpyrght.setTypeface(fs);
		cpyrght.setTypeface(null, Typeface.NORMAL);
		cpyrght.setPadding(0, 0, 0, 10);
		cpyrght.setGravity(Gravity.CENTER);
		cpyrght.setTextColor(Color.WHITE);
		container.addView(cpyrght);
		return(container);
	}
}
