package com.goparkapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

class ConfirmPayment
{
	public LinearLayout confirmPayment(final Context c, Typeface tf) {
		LinearLayout forButton = new LinearLayout(c);
		LinearLayout.LayoutParams forButtonLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams forButtonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		forButtonLayout.gravity = Gravity.CENTER;
		forButton.setLayoutParams(forButtonLayout);
		forButton.setGravity(Gravity.CENTER);
		Button confirmPaymentButton = new Button(c);
		confirmPaymentButton.setText("CONFIRM PAYMENT");
		confirmPaymentButton.setTypeface(tf);
		confirmPaymentButton.setTextColor(Color.WHITE);
		confirmPaymentButton.setBackgroundColor(Color.parseColor("#1B7A86"));
		confirmPaymentButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(c.getApplicationContext(), Receipt.class);
				c.startActivity(intent);
			}
		});
		forButton.addView(confirmPaymentButton);
		forButtonLayout.setMargins(0, 30, 0, 0);
		confirmPaymentButton.setLayoutParams(forButtonLayoutParams);
		return(forButton);
	}
}
