package com.goparkapp;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import java.util.ArrayList;

class ExpirationFieldYear
{
	public LinearLayout forExpirationFieldYear(Context c) {
		LinearLayout expirationFieldLayout = new LinearLayout(c);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams forExpirationFieldLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		forExpirationFieldLayout.setMargins(20, 0, 0, 0);
		expirationFieldLayout.setLayoutParams(lp);
		String[] years = {"2018", "2019", "2020", "2021", "2022", "2023"};
		ArrayList<String> year = new ArrayList<String>();
		for(int i=0; i<years.length; i++) {
			year.add(years[i]);
		}
		Spinner yearChoices = new Spinner(c, Spinner.MODE_DROPDOWN);
		yearChoices.setPopupBackgroundResource(R.drawable.spinnerbackground);
		ArrayAdapter<String> yearsChoices = new ArrayAdapter<String>(c, android.R.layout.simple_spinner_item, year);
		yearChoices.setAdapter(yearsChoices);
		yearsChoices.setDropDownViewResource(android.R.layout.simple_spinner_item);
		yearChoices.setLayoutParams(forExpirationFieldLayout);
		expirationFieldLayout.addView(yearChoices);
		return(expirationFieldLayout);
	}
}
