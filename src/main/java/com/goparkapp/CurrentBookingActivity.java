package com.goparkapp;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Gravity;
import android.view.MotionEvent;
import android.graphics.Typeface;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;
import android.widget.Button;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.EditText;
import android.text.InputType;
import android.text.InputFilter;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.content.Intent;

public class CurrentBookingActivity extends NavigationDrawer
{
	private RecentBookingsAdapter rba;
	private DialogFragment stf;
	private FragmentManager fm;
	private Typeface typeface;
	private RelativeLayout copyright;
	private LinearLayout headers;
	private LinearLayout timePicker;
	private LinearLayout listLayout;
	private LinearLayout crrntBookingInfoLayout;
	private LinearLayout extendLayout;
	private LinearLayout bookNewLayout;
	private ListView lv;
	private ImageButton srchBtn;
	private TextView crrntBooking;
	private EditText extendField;
	private EditText bookNewField;
	private EditText srchBar;
	private double dpWidth;
	private Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.current_booking_layout);
		LinearLayout mainLayout = (LinearLayout)findViewById(R.id.currentbooking_layout);;
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.addView(headers());
		mainLayout.addView(crrntBookingInfo());
		mainLayout.addView(timePicker());
		mainLayout.addView(recentBookings());
		mainLayout.addView(copyright());
		mainLayout.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
					if(srchBar.getVisibility() == View.VISIBLE) {
						srchBar.setText("");
						srchBar.setVisibility(View.GONE);
						crrntBooking.setVisibility(View.VISIBLE);
					}
				}
				return true;
			}
		});
	}

	public LinearLayout headers() {
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		headers = new LinearLayout(this);
		RelativeLayout rl = (RelativeLayout)findViewById(R.id.relativeLayout);
		crrntBooking = (TextView)findViewById(R.id.crrntBooking);
		srchBar = (EditText)findViewById(R.id.searchField);
		srchBar.setSingleLine();
		srchBar.setTextSize(14);
		srchBar.setFilters(new InputFilter[]{new InputFilter.
			LengthFilter(50)});
		crrntBooking.setTypeface(typeface);
		crrntBooking.setTypeface(null, Typeface.BOLD);
		crrntBooking.setTextColor(Color.WHITE);
		crrntBooking.setTextSize(20);
		crrntBooking.setPadding(40, 0, 0, 0);
		srchBtn = (ImageButton)findViewById(R.id.srchButton);
		srchBtn.setImageResource(R.drawable.whitesearch);
		srchBtn.setBackgroundResource(0);
		srchBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(crrntBooking.getVisibility() == View.VISIBLE) {
					srchBar.setText("");
					srchBar.setVisibility(View.VISIBLE);
					crrntBooking.setVisibility(View.GONE);
				}
				else {
					srchBar.setText("");
					srchBar.setVisibility(View.GONE);
					crrntBooking.setVisibility(View.VISIBLE);
				}
			}
		});
		headers.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		return(headers);
	}

	public LinearLayout crrntBookingInfo() {
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		crrntBookingInfoLayout = new LinearLayout(this);
		crrntBookingInfoLayout.setOrientation(LinearLayout.VERTICAL);
		TextView mallName = new TextView(this);
		mallName.setTypeface(typeface);
		mallName.setTypeface(null, Typeface.BOLD);
		mallName.setText("Mall 1");
		mallName.setTextColor(Color.WHITE);
		mallName.setTextSize(18);
		mallName.setGravity(Gravity.CENTER);
		mallName.setPadding(0, 0, 0, 10);
		TextView address = new TextView(this);
		address.setText("Seaside Blvd, 123 MM, Pasay, 1300 Metro Manila");
		address.setTypeface(typeface);
		address.setTextSize(14);
		address.setTextColor(Color.WHITE);
		address.setGravity(Gravity.CENTER);
		TextView starts = new TextView(this);
		starts.setText("Parking starts 9:00");
		starts.setTypeface(typeface);
		starts.setTextSize(14);
		starts.setTextColor(Color.WHITE);
		starts.setGravity(Gravity.CENTER);
		TextView ends = new TextView(this);
		ends.setText("Parking ends 11:00");
		ends.setTextSize(14);
		ends.setTextColor(Color.WHITE);
		ends.setGravity(Gravity.CENTER);
		ends.setTypeface(typeface);
		crrntBookingInfoLayout.addView(mallName);
		crrntBookingInfoLayout.addView(address);
		crrntBookingInfoLayout.addView(starts);
		crrntBookingInfoLayout.addView(ends);
		crrntBookingInfoLayout.setPadding(0, 0, 0, 20);
		return(crrntBookingInfoLayout);
	}

	public LinearLayout timePicker() {
		timePicker = new LinearLayout(this);
		timePicker.setOrientation(LinearLayout.VERTICAL);
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		fm = getSupportFragmentManager();
		extendLayout = new LinearLayout(this);
		extendLayout.setGravity(Gravity.CENTER);
		extendField = new EditText(this);
		extendField.setText("EXTEND PARKING");
		extendField.setTextColor(Color.WHITE);
		extendField.setInputType(InputType.TYPE_NULL);
		extendField.setWidth(500);
		extendField.setBackgroundColor(Color.parseColor("#1B7A86"));
		extendField.setGravity(Gravity.CENTER);
		extendField.setTypeface(typeface);
		extendField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stf = new SetTimeFragment(extendField);
				stf.show(fm.beginTransaction(), "time picker");
			}
		});
		extendLayout.setPadding(0, 0, 0, 10);
		bookNewLayout = new LinearLayout(this);
		bookNewLayout.setGravity(Gravity.CENTER);
		bookNewField = new EditText(this);
		bookNewField.setText("BOOK NEW PARKING");
		bookNewField.setTextColor(Color.WHITE);
		bookNewField.setInputType(InputType.TYPE_NULL);
		bookNewField.setWidth(500);
		bookNewField.setBackgroundColor(Color.parseColor("#1B7A86"));
		bookNewField.setGravity(Gravity.CENTER);
		bookNewField.setTypeface(typeface);
		bookNewField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(CurrentBookingActivity.this, HomeScreenActivity.class));
			}
		});
		bookNewLayout.setPadding(0, 0, 0, 50);
		extendLayout.addView(extendField);
		bookNewLayout.addView(bookNewField);
		timePicker.addView(extendLayout);
		timePicker.addView(bookNewLayout);
		return(timePicker);
	}

	public LinearLayout recentBookings() {
		typeface = Typeface.createFromAsset(getAssets(),"font/OpenSans.ttf");
		LinearLayout rbLayout = new LinearLayout(this);
		rbLayout.setOrientation(LinearLayout.VERTICAL);
		TextView rb = new TextView(this);
		rb.setTypeface(typeface);
		rb.setTypeface(null, Typeface.BOLD);
		rb.setTextColor(Color.WHITE);
		rb.setText("Recent Bookings");
		rb.setTextSize(14);
		rb.setGravity(Gravity.CENTER);
		TextView mn = new TextView(this);
		mn.setBackgroundColor(Color.WHITE);
		mn.setTextColor(Color.parseColor("#3FB0AC"));
		mn.setText("Mall Name");
		String mallN = mn.getText().toString();
		TextView d = new TextView(this);
		d.setBackgroundColor(Color.WHITE);
		d.setTextColor(Color.parseColor("#3FB0AC"));
		d.setText("Date");
		String dt = d.getText().toString();
		TextView tId = new TextView(this);
		tId.setBackgroundColor(Color.WHITE);
		tId.setTextColor(Color.parseColor("#3FB0AC"));
		tId.setText("Transaction ID");
		String ti = tId.getText().toString();
		String[] mallNames = { mallN, "MOA", "MegaMall", "Ayala Mall", "Nuvali" };
		String[] date = { dt, "08/09/2018", "08/09/2018", "08/09/2018", "08/09/2018", "08/09/2018" };
		String[] transactId = { ti, "WJ8EFY8E6H", "W50LHPHKT0", "POSOE9OG92", "SO4FJR43LH", "LD03IFSQWE" };
		rba = new RecentBookingsAdapter(this, mallNames, date, transactId);
		lv = new ListView(this);
		lv.setAdapter(rba);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String item = (String) lv.getItemAtPosition(position);
				Toast.makeText(getApplicationContext(), item, Toast.LENGTH_SHORT).show();
			}
		});
		rbLayout.addView(rb);
		rbLayout.addView(lv);
		return(rbLayout);
	}

	public RelativeLayout copyright() {
		copyright = new RelativeLayout(this);
		TextView cp = new TextView(this);
		cp.setText("2018 GoPark Philippines. All rights reserved.");
		cp.setTextSize(12);
		cp.setTextColor(Color.WHITE);
		cp.setPadding(0, 0, 0, 10);
		copyright.addView(cp);
		LayoutParams copyrightParams = (LayoutParams)cp.getLayoutParams();
		copyrightParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		copyrightParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		return(copyright);
	}
}
