package com.goparkapp;

import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

class ThreeImagesLayoutParams
{
	public RelativeLayout imagesLayoutParams(VisaPaymentMethod visa, MasterCardPaymentMethod masterCard, PaypalPaymentMethod paypal, RelativeLayout threeImages, GradientDrawable gd, GradientDrawable gd2, GradientDrawable gd3, ImageView visaImage, ImageView masterCardImage, ImageView paypalImage, ImageView visaClickEvent, ImageView masterCardClickEvent, ImageView paypalClickEvent) {
		RelativeLayout rl = threeImages;
		rl.addView(visaClickEvent);
		rl.addView(masterCardClickEvent);
		rl.addView(paypalClickEvent);
		LayoutParams forVisaImage = (LayoutParams)visaClickEvent.getLayoutParams();
		forVisaImage.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams forMasterCardImage = (LayoutParams)masterCardClickEvent.getLayoutParams();
		forMasterCardImage.addRule(RelativeLayout.CENTER_HORIZONTAL);
		forMasterCardImage.addRule(RelativeLayout.CENTER_IN_PARENT);
		LayoutParams forPaypalImage = (LayoutParams)paypalClickEvent.getLayoutParams();
		forPaypalImage.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		rl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		return(rl);
	}
}
