package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TextView;

class EnterEmailAddressLabel
{
	public TextView enterEmailAddressLabel(Context c, Typeface tf) {
		TextView emailAddressLabel = new TextView(c);
		emailAddressLabel.setText("Enter your account email address.");
		emailAddressLabel.setTypeface(tf);
		emailAddressLabel.setTextSize(14);
		emailAddressLabel.setTextColor(Color.WHITE);
		return(emailAddressLabel);
	}
}
