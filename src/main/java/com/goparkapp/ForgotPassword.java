package com.goparkapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class ForgotPassword extends Activity
{
	int wrapContent;
	int matchParent;

	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
		matchParent = LinearLayout.LayoutParams.MATCH_PARENT;
		Typeface tf = Typeface.createFromAsset(getAssets(), "font/OpenSans.ttf");
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		Activity thisClass = (Activity)this;
		BackButtonImage back = new BackButtonImage(thisClass, this);
		ImageView backButton = back.backButtonImage();
		GoParkLogo logo = new GoParkLogo();
		ImageView goParkImage = logo.goParkLogo(this, wrapContent);
		RelativeLayout forImages = new RelativeLayout(this);
		forImages.addView(backButton);
		forImages.addView(goParkImage);
		LayoutParams forBackButton = (LayoutParams)backButton.getLayoutParams();
		forBackButton.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams forGoParkImage = (LayoutParams)goParkImage.getLayoutParams();
		forGoParkImage.addRule(RelativeLayout.CENTER_IN_PARENT);
		forImages.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		mainLayout.addView(forImages);
		DisplayMetrics width = getResources().getDisplayMetrics();
		double limitWidth = width.widthPixels*0.7;
		LinearLayout limit = new LinearLayout(this);
		limit.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams forLimit = linearLayouts((int)limitWidth, matchParent);
		forLimit.gravity = Gravity.CENTER;
		limit.setLayoutParams(forLimit);
		ForgotPasswordLabel fPasswordLabel = new ForgotPasswordLabel();
		LinearLayout forForgotPasswordLabel = new LinearLayout(this);
		forForgotPasswordLabel.setGravity(Gravity.CENTER);
		LinearLayout.LayoutParams forForgotPasswordLabelLayout = linearLayouts(matchParent, wrapContent);
		forForgotPasswordLabel.setLayoutParams(forForgotPasswordLabelLayout);
		forForgotPasswordLabel.addView(fPasswordLabel.forgotPasswordLabel(this, tf));
		limit.addView(forForgotPasswordLabel);
		EnterEmailAddressLabel emailAddressLabel = new EnterEmailAddressLabel();
		LinearLayout forEmailAddressLabel = new LinearLayout(this);
		forEmailAddressLabel.setGravity(Gravity.CENTER);
		LinearLayout.LayoutParams forEmailAddressLabelLayout = linearLayouts(matchParent, wrapContent);
		forEmailAddressLabelLayout.setMargins(0, 100, 0, 0);
		forEmailAddressLabel.setLayoutParams(forEmailAddressLabelLayout);
		forEmailAddressLabel.addView(emailAddressLabel.enterEmailAddressLabel(this, tf));
		limit.addView(forEmailAddressLabel);
		DisplayMetrics buttonWidth = getResources().getDisplayMetrics();
		double forButtonWidth = buttonWidth.widthPixels*0.7;
		ChangeButton button = new ChangeButton();
		LinearLayout.LayoutParams buttonParams = linearLayouts((int)forButtonWidth-90, wrapContent);
		Button bb = button.changeButton(this, buttonParams);
		ForgotPasswordTextField passwordTextField = new ForgotPasswordTextField();
		LinearLayout forForgotPasswordTextField = new LinearLayout(this);
		LinearLayout.LayoutParams forForgotPasswordTextFieldLayout = linearLayouts(matchParent, wrapContent);
		forForgotPasswordTextFieldLayout.setMargins(0, 25, 0, 0);
		forForgotPasswordTextField.setLayoutParams(forForgotPasswordTextFieldLayout);
		forForgotPasswordTextField.addView(passwordTextField.forgotPasswordTextField(this, tf, matchParent, wrapContent, bb));
		limit.addView(forForgotPasswordTextField);
		buttonParams.setMargins(0, 25, 0, 0);
		buttonParams.gravity = Gravity.CENTER;
		limit.addView(bb);
		Copyright c = new Copyright();
		LinearLayout forCopyright = new LinearLayout(this);
		forCopyright.setGravity(Gravity.BOTTOM);
		LinearLayout.LayoutParams forCopyrightLayout = linearLayouts(matchParent, matchParent);
		forCopyrightLayout.setMargins(0, 0, 0, 10);
		forCopyright.setLayoutParams(forCopyrightLayout);
		forCopyrightLayout.gravity = Gravity.CENTER;
		forCopyright.addView(c.copyright(this, tf));
		limit.addView(forCopyright);
		mainLayout.addView(limit);
		setContentView(mainLayout);
	}

	public LinearLayout.LayoutParams linearLayouts(int width, int height) {
		return(new LinearLayout.LayoutParams(width, height));
	}
}
