package com.goparkapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

class Popup
{
	Context c = null;
	Typeface tf = null;

	public Popup(Context c, Typeface tf) {
		this.c = c;
		this.tf = tf;
	}

	public void maximumCharPopup(String text) {
		AlertDialog.Builder maxCharPopup = new AlertDialog.Builder(c);
		maxCharPopup.setMessage(Html.fromHtml("<font color='#3FB0AC' family='" + tf + "'>" + text + "</font>"));
		maxCharPopup.setPositiveButton("OK",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {}
		});
		AlertDialog button = maxCharPopup.create();
		button.show();
		Button doneButton = button.getButton(DialogInterface.BUTTON_POSITIVE);
		doneButton.setBackgroundColor(Color.parseColor("#1B7A86"));
		doneButton.setTextColor(Color.WHITE);
		doneButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
	}
}
