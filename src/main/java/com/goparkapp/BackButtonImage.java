package com.goparkapp;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

class BackButtonImage
{
	Context c;
	Activity thisClass = null;

	public BackButtonImage(Activity thisClass, Context c) {
		this.c = c;
		this.thisClass = thisClass;
	}

	public ImageView backButtonImage() {
		ImageView backButton = new ImageView(c);
		final Activity a = thisClass;
		backButton.setImageResource(R.drawable.back);
		LinearLayout.LayoutParams imageSize = new LinearLayout.LayoutParams(36, 36);
		backButton.setLayoutParams(imageSize);
		backButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				a.finish();
			}
		});
		return(backButton);
	}
}
