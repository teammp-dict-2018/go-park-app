package com.goparkapp;

import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

class Limit
{
	public LinearLayout limitAll(Context c, Typeface tf, CardNumberLabel cnl, CardNumberField cnf, ExpirationDateLabel edl, ExpirationFields ef, CvcAndCardHoldersNameLabel cachnl, CvcAndCardHoldersNameTextField textFields, ConfirmPayment cp) {
		DisplayMetrics width = c.getResources().getDisplayMetrics();
		double screenWidth = width.widthPixels;
		LinearLayout limit = new LinearLayout(c);
		LinearLayout.LayoutParams forLimit = new LinearLayout.LayoutParams((int)screenWidth-150, LinearLayout.LayoutParams.MATCH_PARENT);
		forLimit.gravity = Gravity.CENTER;
		limit.setLayoutParams(forLimit);
		limit.setOrientation(LinearLayout.VERTICAL);
		limit.addView(cnl.cardNumberLabel(c, tf));
		limit.addView(cnf.cardNumberField(c));
		limit.addView(edl.expirationDateLabel(c, tf));
		limit.addView(ef.expirationField(c));
		limit.addView(cachnl.forCvcAndCardLabel(c, tf));
		limit.addView(textFields.allTextFields(c));
		limit.addView(cp.confirmPayment(c, tf));
		return(limit);
	}
}