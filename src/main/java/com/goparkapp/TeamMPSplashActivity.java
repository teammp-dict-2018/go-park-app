package com.goparkapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TeamMPSplashActivity extends AppCompatActivity {
	private static int SPLASH_TIME_OUT = 1500;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_teammp_splash);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent loginActivity = new Intent(TeamMPSplashActivity.this, LoginActivity.class);
				startActivity(loginActivity);
				overridePendingTransition(R.anim.fadein, R.anim.fadeout);
				finish();
			}
		}, SPLASH_TIME_OUT);
	}
}
