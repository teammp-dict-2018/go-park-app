package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.widget.TextView;

class Copyright
{
	public TextView copyright(Context c, Typeface tf) {
		TextView copyrightLabel = new TextView(c);
		copyrightLabel.setText("2018 GoPark Philippines. All Rights Reserved");
		copyrightLabel.setTypeface(tf);
		copyrightLabel.setTextSize(12);
		copyrightLabel.setTextColor(Color.WHITE);
		return(copyrightLabel);
	}
}
