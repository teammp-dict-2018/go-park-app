package com.goparkapp;

import android.content.Context;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;

class ExpirationFields
{
	public LinearLayout expirationField(Context c) {
		LinearLayout forExpirationFields = new LinearLayout(c);
		forExpirationFields.setOrientation(LinearLayout.HORIZONTAL);
		DisplayMetrics width = c.getResources().getDisplayMetrics();
		double screenWidth = width.widthPixels;
		LinearLayout.LayoutParams fieldsLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		forExpirationFields.setLayoutParams(fieldsLayout);
		RelativeLayout expirationFieldsLayout = new RelativeLayout(c);
		ExpirationFieldMonth efm = new ExpirationFieldMonth();
		LinearLayout forSpinnerMonth = efm.monthDropDown(c);
		ExpirationFieldYear efy = new ExpirationFieldYear();
		LinearLayout forSpinnerYear = efy.forExpirationFieldYear(c);
		expirationFieldsLayout.addView(forSpinnerMonth);
		expirationFieldsLayout.addView(forSpinnerYear);
		LayoutParams forSpinnerMonthLayout = (LayoutParams)forSpinnerMonth.getLayoutParams();
		forSpinnerMonthLayout.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams forSpinnerYearLayout = (LayoutParams)forSpinnerYear.getLayoutParams();
		forSpinnerYearLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		expirationFieldsLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		forExpirationFields.addView(expirationFieldsLayout);
		return(forExpirationFields);
	}
}
