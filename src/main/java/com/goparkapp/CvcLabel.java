package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TextView;

class CvcLabel
{
	public TextView cvcLabel(Context c, Typeface tf) {
		TextView cvc = new TextView(c);
		cvc.setText("CVC");
		cvc.setTypeface(tf);
		cvc.setTextSize(14);
		cvc.setTextColor(Color.WHITE);
		cvc.setPadding(0, 30, 0, 15);
		return(cvc);
	}
}
