package com.goparkapp;

import android.view.LayoutInflater;
import android.widget.Toast;
import android.view.View;
import android.content.Context;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.graphics.Color;
import android.widget.TableRow.LayoutParams;
import android.widget.TableRow;
import android.graphics.Typeface;
import android.view.Gravity;

public class MallListAdapter extends ArrayAdapter<String>
{
	private Context context;
	private String[] mallNames;
	private String[] parkingTimes;
	private Typeface typeface;

	public MallListAdapter(Context c, String[] mn, String[] pt) {
		super(c, R.layout.mall_list_layout, mn);
		context = c;
		mallNames = mn;
		parkingTimes = pt;
	}

	public View getView(int pos, View view, ViewGroup parent) {
		typeface = Typeface.createFromAsset(context.getAssets(),"font/OpenSans.ttf");
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.mall_list_layout, null, true);
		TableRow.LayoutParams nameParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		nameParams.setMargins(10, 20, 0, 20);
		nameParams.gravity = Gravity.CENTER;
		TextView mallName = (TextView) rowView.findViewById(R.id.mallname);
		mallName.setLayoutParams(nameParams);
		mallName.setTypeface(typeface);
		mallName.setWidth(400);
		TableRow.LayoutParams availParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		availParams.setMargins(10, 20, 0, 20);
		availParams.gravity = Gravity.CENTER;
		TextView parkingTime = (TextView) rowView.findViewById(R.id.parkingtime);
		parkingTime.setLayoutParams(availParams);
		parkingTime.setTypeface(typeface);
		parkingTime.setWidth(400);
		mallName.setText(mallNames[pos]);
		mallName.setTextColor(Color.WHITE);
		parkingTime.setText(parkingTimes[pos]);
		parkingTime.setTextColor(Color.WHITE);
		return(rowView);
	}
}
