package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Typeface;
import android.widget.TextView;

class ExpirationDateLabel
{
	public TextView expirationDateLabel(Context c, Typeface tf) {
		TextView expirationLabel = new TextView(c);
		expirationLabel.setText("EXPIRATION DATE");
		expirationLabel.setTypeface(tf);
		expirationLabel.setTextSize(14);
		expirationLabel.setTextColor(Color.WHITE);
		expirationLabel.setPadding(0, 40, 0, 15);
		return(expirationLabel);
	}
}
