package com.goparkapp;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

class ThreeImages
{
	Context c;

	public ThreeImages(Context c) {
		this.c = c;
	}

	public LinearLayout forThreeImages(LinearLayout mainLayout, CardNumberLabel cnl, CardNumberField cnf, Typeface tf, ExpirationDateLabel edl, ExpirationFields ef, CvcAndCardHoldersNameLabel cachnl, CvcAndCardHoldersNameTextField textFields, ConfirmPayment cp) {
		DisplayMetrics width = c.getResources().getDisplayMetrics();
		double limitWidth = width.widthPixels;
		LinearLayout.LayoutParams forLimit = new LinearLayout.LayoutParams((int)limitWidth-150, LinearLayout.LayoutParams.MATCH_PARENT);
		forLimit.gravity = Gravity.CENTER;
		GradientDrawable gd = new GradientDrawable();
		GradientDrawable gd2 = new GradientDrawable();
		GradientDrawable gd3 = new GradientDrawable();
		VisaPaymentMethod visa = new VisaPaymentMethod(c);
		MasterCardPaymentMethod masterCard = new MasterCardPaymentMethod(c);
		PaypalPaymentMethod paypal = new PaypalPaymentMethod(c);
		LinearLayout limit = new LinearLayout(c);
		limit.setLayoutParams(forLimit);
		RelativeLayout threeImages = new RelativeLayout(c);
		ImageView visaImage = visa.visaImage();
		ImageView masterCardImage = masterCard.masterCardImage();
		ImageView paypalImage = paypal.paypalImage();
		ThreeImagesLayoutParams tilp = new ThreeImagesLayoutParams();
		limit.addView(tilp.imagesLayoutParams(visa, masterCard, paypal, threeImages, gd, gd2, gd3, visaImage, masterCardImage, paypalImage, visa.visaClickEvent(visaImage, masterCardImage, paypalImage, gd, gd2, gd3, mainLayout, cnl, cnf, tf, edl, ef, cachnl, textFields, cp), masterCard.masterCardClickEvent(masterCardImage, visaImage, paypalImage, gd, gd2, gd3), paypal.paypalImageClickEvent(paypalImage, visaImage, masterCardImage, gd, gd2, gd3)));
		return(limit);
	}
}
