package com.goparkapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

class CardNumberLabel
{
	public TextView cardNumberLabel(Context c, Typeface tf) {
		TextView cardLabel = new TextView(c);
		cardLabel.setText("CARD NUMBER");
		cardLabel.setTypeface(tf);
		cardLabel.setTextSize(14);
		cardLabel.setTextColor(Color.WHITE);
		cardLabel.setPadding(0, 15, 0, 10);
		return(cardLabel);
	}
}
