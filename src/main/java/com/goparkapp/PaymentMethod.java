package com.goparkapp;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class PaymentMethod extends Activity
{
	@Override
	public void onCreate(Bundle b) {
		super.onCreate(b);
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setBackgroundColor(Color.parseColor("#3FB0AC"));
		Copyright cc = new Copyright();
		Typeface tf = Typeface.createFromAsset(getAssets(), "font/OpenSans.ttf");
		LinearLayout forColorLayout = new LinearLayout(this);
		forColorLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams forMainLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200);
		forColorLayout.setBackgroundColor(Color.parseColor("#1B7A86"));
		Activity thisClass = (Activity)this;
		BackButtonImage back = new BackButtonImage(thisClass, this);
		PaymentMethodLabel paymentMethod = new PaymentMethodLabel(this);
		RelativeLayout forBackButtonAndPaymentLabel = new RelativeLayout(this);
		ImageView backImage = back.backButtonImage();
		TextView paymentLabel = paymentMethod.paymentMethodLabel();
		forBackButtonAndPaymentLabel.addView(backImage);
		forBackButtonAndPaymentLabel.addView(paymentLabel);
		forBackButtonAndPaymentLabel.setPadding(10, 10, 10, 10);
		LayoutParams forBackButton = (LayoutParams)backImage.getLayoutParams();
		forBackButton.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams forPaymentMethod = (LayoutParams)paymentLabel.getLayoutParams();
		forPaymentMethod.addRule(RelativeLayout.CENTER_HORIZONTAL);
		forPaymentMethod.addRule(RelativeLayout.CENTER_IN_PARENT);
		forBackButtonAndPaymentLabel.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		forColorLayout.addView(forBackButtonAndPaymentLabel);
		CardNumberLabel cnl = new CardNumberLabel();
		CardNumberField cnf = new CardNumberField();
		ExpirationDateLabel edl = new ExpirationDateLabel();
		ExpirationFields ef = new ExpirationFields();
		CvcAndCardHoldersNameLabel cachnl = new CvcAndCardHoldersNameLabel();
		CvcAndCardHoldersNameTextField textFields = new CvcAndCardHoldersNameTextField();
		ConfirmPayment cp = new ConfirmPayment();
		ThreeImages ti = new ThreeImages(this);
		forColorLayout.addView(ti.forThreeImages(mainLayout, cnl, cnf, tf, edl, ef, cachnl, textFields, cp));
		mainLayout.addView(forColorLayout, forMainLayout);
		setContentView(mainLayout);
	}
}
