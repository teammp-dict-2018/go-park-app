package com.goparkapp;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import java.util.Calendar;
import android.widget.TimePicker;
import android.widget.EditText;
import android.content.Context;
import java.util.Calendar;

public class SetTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener
{
	EditText et;

	public SetTimeFragment(EditText et) {
		this.et = et;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState){
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		return(new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity())));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute){
		et.setText(hourOfDay + " : " + minute);
	}
}
