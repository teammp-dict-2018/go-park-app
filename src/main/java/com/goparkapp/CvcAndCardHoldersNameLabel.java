package com.goparkapp;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

class CvcAndCardHoldersNameLabel
{
	public RelativeLayout forCvcAndCardLabel(Context c, Typeface tf) {
		CvcLabel cvc = new CvcLabel();
		CardHoldersNameLabel chnl = new CardHoldersNameLabel();
		RelativeLayout labelsLayout = new RelativeLayout(c);
		TextView cvcLabel = cvc.cvcLabel(c, tf);
		TextView cardNameLabel = chnl.cardHoldersNameLabel(c, tf);
		labelsLayout.addView(cvcLabel);
		labelsLayout.addView(cardNameLabel);
		LayoutParams forCvcLabel = (LayoutParams)cvcLabel.getLayoutParams();
		forCvcLabel.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		LayoutParams forCardNameLabel = (LayoutParams)cardNameLabel.getLayoutParams();
		forCardNameLabel.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		labelsLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		return(labelsLayout);
	}
}
