package com.goparkapp;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.EditText;
import android.graphics.Color;
import android.text.InputFilter;

public class AccountSettingsActivity extends NavigationDrawer
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreateDrawer(R.layout.account_settings_layout);
		LinearLayout accountLayout = (LinearLayout) findViewById(R.id.account_layout);
		accountLayout.setOrientation(LinearLayout.VERTICAL);
		EditText address = (EditText) findViewById(R.id.address_txt);
		address.setSingleLine();
		address.setFilters(new InputFilter[]{new InputFilter.LengthFilter(60)});
		accountLayout.setOrientation(LinearLayout.VERTICAL);
	}
}