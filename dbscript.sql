DROP TABLE IF EXISTS goparkusers;
CREATE TABLE goparkusers (
	userid INT PRIMARY KEY NOT NULL AUTO_INCREMENT, firstname VARCHAR(20) NOT NULL, lastname VARCHAR(20) NOT NULL, username VARCHAR(20) NOT NULL, password VARCHAR(20) NOT NULL, email VARCHAR(30) NOT NULL, address VARCHAR(255) NOT NULL, vehicletype VARCHAR(10) NOT NULL, phone INT, platenumber VARCHAR(10) NOT NULL, paymentmethod VARCHAR(30)
);
INSERT INTO goparkusers (firstname, lastname, username, password, email, address, vehicletype, platenumber) VALUES ("Ernst", "Legaspi", "sylar", "12345abc", "ernst.legaspi@gmail.com", "Paranaque City", "Car", "ABC-1235");
INSERT INTO goparkusers (firstname, lastname, username, password, email, address, vehicletype, platenumber) VALUES ("Kenneth", "Paro", "kparrot", "12345678", "kenneth.paro@yahoo.com", "Paranaque City", "Motorcycle", "CBD-3243");